<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //customer
    'home'               => 'ទំព័រដើម',
    'list_all_customer'  => 'បង្ហាញអតិថិជនទាំងអស់',
    'customer'           => 'អតិថិជន',
    'no'                 => 'ល.រ',
    'customer_phone'     => 'លេខទូរស័ព្ទអតិថិជន',
    'customer_name'      => 'ឈ្មោះអតិថិជន',
    'fre_order'          => 'ចំនួនដឹកសរុប',
    'first_order'        => 'ថ្ងៃដឹកដំបូង',
    'last_order'         => 'ថ្ងៃដឹកចុងក្រោយ',
    'order'              => 'បញ្ជា',
    'action'             => 'សកម្មភាព',
    'add_order'          => 'បន្ថែមការដឹកជញ្ចូន',
    'view_history'       => 'បង្ហាញប្រវត្តិ',//ការដឹកជញ្ជូនរបស់ហាងទាំងអស់
    'view'               => 'បង្ហាញ',
    'modify'             => 'ធ្វើបច្ចប្បន្នភាព',
    'delete'             => 'លុប',

    'destination_zone'            => 'តំបន់គោលដៅ',
    'please_select_zone'          => 'សូមជ្រើសរើសតំបន់',
    'destination_phone'           => 'លេខទូរស័ព្ទគោលដៅ',
    'destination_desc'            => 'បរិយាយ',
    'type_of_service'             => 'ប្រភេទសេវាកម្ម',
    'any_extra_service_charge'    => 'គិតថ្លៃសេវាកម្មបន្ថែម?',
    'extra_amount'                => 'ចំនួនទឹកប្រាក់បន្ថែម',
    'get_payment_at'             => 'ទទួលប្រាក់នៅ?',
    'total_amount'                => 'ចំនួន​សរុប',
    'taxi'                        => 'តាក់ស៊ី',
    'other'                       => 'ផ្សេងទៀត',
    'Yes'                         => 'មាន',
    'No'                           => 'មិនមាន',
    'address'                    => 'អាសយដ្ឋាន',
    'bank_inforamtion'           => 'ព័ត៌មានធនាគារ',
    'bank_name'                  => 'ឈ្មោះ​របស់​ធនាគារ',
    'account_name'               => 'ឈ្មោះ​គណនី',
    'account_number'             => 'លេខ​គណនី',
    'description'                => 'បរិយាយ',
    'status'                     => 'ស្ថានភាព',
    'history'                    => 'ប្រវត្តិ',
    'list_all_history'           => 'បង្ហាញប្រវត្តិទាំងអស់',
    'filter_by_services'         => 'ជ្រើសរើសតាមប្រភេទសេវាកម្ម',
    'filter_by_delivery'         => 'ជ្រើសរើសអ្នកដឹកជញ្ជូន',
    'filter_by_shop'             => 'ជ្រើសរើសហាងដៃគូរ',
    'filter_by_shop_today'       => 'ជ្រើសរើសហាងដៃគូរសម្រាប់ថ្ងៃនេះ',
    'filter_by_year'             => 'ជ្រើសរើសតាមឆ្នាំ',
    'filter_by_month'            => 'ជ្រើសរើសតាមខែ',

    //manage delivery
    'manage_delivery'                 => 'គ្រប់គ្រងការដឹកជញ្ជូន',
    'list_all_delivery'               => 'បង្ហាញការដឹកជញ្ជូនទាំងអស់',
    'shop_name'                       => 'ឈ្មោះហាង',
    'des_zone'                        => 'តំបន់គោលដៅ',
    'des_phone'                       => 'លេខទូរស័ព្ទ',
    'delivery_by'                     => 'អ្នកដឹក',
    'delivery_status'                 => 'ស្ថានភាពដឹក',
    'action_detail'                   => 'សកម្មភាព',
    'entry_by'                        => 'បញ្ជូលដោយ',
    'assign'                          => 'ជ្រើសរើសអ្នកដឹក',
    'please_assign_delivery'          => 'ជ្រើសរើសអ្នកដឹកថ្មី',
    'modify_delivery'                 => 'ធ្វើបច្ចប្បន្នភាពអ្នកដឹក',
    'delivery_arrang'                 => 'ការរៀបចំការដឹកជញ្ជូន',
    'print_out'                       => 'បោះពុម្ព',

    //clear payment staff
    'list_all_payment_collection_with_staff' => 'ទូទាត់ប្រាក់ទាំងអស់ជាមួយបុគ្គលិក',
    'list_all_payment_collection_with_shop'  => 'ទូទាត់ប្រាក់ទាំងអស់ជាមួយហាងដៃគូរ',
    'progress'                               => 'កំពុងដំណើរការ',
    'complete'                               => 'បញ្ចប់',

    //print
    'check_list'                              => 'តារាងពិនិត្យ',
    'check'                                   => 'ពិនិត្យ',
    'price_usd'                               => 'តម្លៃ',
    'price_kh'                                => 'តម្លៃ',

    
     //print out
     'code_no'                                            => 'លេខកូដ',
     'zone_desc'                                          => 'ទីតាំងបរិយាយ',
     'type_delivery'                                      => 'ប្រភេទនៃការដឹក',
     'amount_at_staff'                                    => 'ប្រាក់នៅបុគ្គលិក',
     'amount_at_company'                                  => 'ប្រាក់នៅក្រុមហ៊ុន',
     'amount_at_seller'                                   => 'ប្រាក់នៅអ្នកលក់',
     'total_amount_paid'                                  => 'ប្រាក់ដែលត្រូវទូទាត់',
     'invoices_for_delivery'                              => 'វិក្កយបត្រ (សំរាប់អ្នកដឹក)',
     'delivery_name'                                      => 'ឈ្មោះអ្នកដឹកជញ្ជុន',
     'phone_number'                                       => 'លេខទូរសព្ទ',
     'code'                                               => 'លេខ',
     'h_p'                                                => 'H/P',
     'name'                                               => 'ឈ្មោះ',



    //zone type
    'list_all_zone_type'     => 'បង្ហាញប្រភេទតំបន់ទាំងអស់',
    'zone_type'              => 'ប្រភេទតំបន់',
    'add_new'                => 'បន្ថែមថ្មី',
    'back'                   => 'ត្រឡប់',
    'reset'                  => 'កំណត់ឡើងវិញ',
    'save'                   => 'រក្សារុក',
    'date'                   => 'កាលបរិច្ឆេទ',
    'name_en'                => 'ឈ្មោះប្រភេទតំបន់អង់គ្លេស',
    'name_kh'                => 'ឈ្មោះប្រភេទតំបន់ខ្មែរ',

      //commission
      'commission'            => 'កម្រៃជើងផ្សារ',
      'set_up_a_commission'   => 'រៀបដាក់កម្រៃជើងផ្សារ',
      'commission_fee'        => 'កម្រៃជើងសារ',
      'service_express'       => 'សេវាកម្មអ៊ិចប្រេស',
      'commission_required'   => 'កម្រៃជើងសារត្រូវតែបំពេញ',

    //zone
    'list_all_zone'           => 'បង្ហាញតំបន់ទាំងអស់',
    'zone'                    => 'តំបន់',
    'zone_name_en'            => 'ឈ្មោះតំបន់អង់គ្លេស',
    'zone_name_kh'            => 'ឈ្មោះតំបន់ខ្មែរ',
    'service_fee'             => 'តម្លៃដឹក',

     //department
     'list_all_department'           => 'បង្ហាញផ្នែកទាំងអស់',
     'department'                    => 'ផ្នែក',
     'department_name_en'            => 'ឈ្មោះផ្នែកអង់គ្លេស',
     'department_name_kh'            => 'ឈ្មោះផ្នែកខ្មែរ',

      //position
    'list_all_position'             => 'បង្ហាញមុខតំណែងទាំងអស់',
    'position'                      => 'មុខតំណែង',
    'position_name_en'              => 'ឈ្មោះមុខតំណែងអង់គ្លេស',
    'position_name_kh'              => 'ឈ្មោះមុខតំណែងខ្មែរ',

      //Users
      'list_all_users'                => 'បង្ហាញអ្នកប្រើប្រាស់ទាំងអស់',
      'users'                         => 'អ្នកប្រើប្រាស់',
      'email'                         => 'អ៊ីម៊ែល',
      'roles'                          => 'តួនាទី',
      'gender'                        => 'ភេទ',
      'full_name'                     => 'ឈ្មោះពេញ',
      'password'                      => 'លេខសំងាត់',
      'confirm_password'              => 'បញ្ជាក់លេខសំងាត់',
 
    //Roles
    'list_all_roles'                => 'បង្ហាញតួនាទីទាំងអស់',
    'slug'                          => 'Slug',
    
      //company
      'profile'                     => 'ប្រវត្តិ',
      'company'                     => 'ក្រុមហ៊ុន',
      'change_logo'                 => 'ប្តូររូបសញ្ញា',
      'company_name_en'             => 'ឈ្មោះក្រុមហ៊ុនអង់គ្លេស',
      'company_name_kh'             => 'ឈ្មោះក្រុមហ៊ុនខ្មែរ',
      'company_email'               => 'អ៊ីម៉ែលរបស់ក្រុមហ៊ុន',
      'company_phone'               => 'ទូរស័ព្ទក្រុមហ៊ុន',
      'company_address'             => 'អាសយដ្ឋានក្រុមហ៊ុន',
      'change_profile'              => 'ផ្លាស់ប្តូរទម្រង់',
      'old_password'                => 'លេខសំងាត់​ចាស់',
      'personal_data'               => 'ទិន្នន័យ​ផ្ទាល់ខ្លួន',
      'security'                    => 'សុវត្ថិភាពគណនី',

       //lang
     'language'                    => 'ភាសា',
     'english'                     => 'អង់គ្លេស',
     'khmer'                       => 'ខ្មែរ',
     'account'                     => 'គណនី',
     'logout'                      => 'ចាកចេញ',
     'hi'                          => 'សួស្ដី',
     'wrong_old_password'          => 'ពាក្យសម្ងាត់ចាស់ខុស',
     


    //message
    'updated_successfully'   => 'ធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
    'save_successfully'      => 'រក្សាទុកដោយជោគជ័យ',
    'delete_successfully'    => 'លុបដោយជោគជ័យ',

    //datatable 
    'no_matching_records_found' => 'មិនមានទិន្នន័យនេះទេ',
    'search'                    => 'ស្វែងរក',
    'display'                   => 'បង្ហាញ',
    'records_per_page'          => 'ក្នុងមួយទំព័រ',
    'showing_page'              => 'បង្ហាញទំព័រទី',
    'of'                        => 'នៃ',
    'previous'                  => 'ទំព័រ​មុន',
    'next'                      => 'បន្ទាប់',
    'no_records_available'      => 'មិនមានទិន្នន័យទេ',
    'filtered_from'             => 'សរុប',
    'total_records'             => 'ទិន្នន័យ',
   
    //menu
    'manage_order'                => 'គ្រប់គ្រងការបញ្ជា',
    'manage_customer'             => 'គ្រប់គ្រងអតិថិជន',
    'manage_delivery'             => 'គ្រប់គ្រងការដឹក',
    'payment_collection'          => 'ការទូទាត់ប្រាក់',
    'clear_payment_staff'         => 'ការទូទាត់ប្រាក់ជាមួយបុគ្គលិក',
    'clear_payment_shop'          => 'ការទូទាត់ប្រាក់ជាមួយហាង',
    'confirm_payment_with_staff'  => 'បញ្ជាក់ការទូទាត់ប្រាក់ជាមួយបុគ្គលិក',
    'confirm_payment_with_shop'   => 'បញ្ជាក់ការទូទាត់ប្រាក់ជាមួយហាង',
    'operation'                   => 'Operation',
    'income_expense'              => 'ចំណូល&ចំណាយ',
    'employee'                    => 'បញ្ជីរបុគ្គលិក',
    'payroll'                     => 'Payroll',
    'daily_expense'               => 'Daily Expense',
    'report'                      => 'របាយការណ៍',
    'income'                      => 'ចំណូល',
    'expense'                     => 'ចំណាយ',
    'shop_partner'                => 'ហ៊ាងដៃគូរ',
    'delivery'                    => 'អ្នកដឹកជញ្ជូន',
    'setting'                     => 'ការកំណត់',
    'company_profile'             => 'ប្រវត្តិ​ក្រុមហ៊ុន',
    'manage_zone_type'            => 'គ្រប់គ្រងប្រភេទតំបន់',
    'manage_commission'           => 'គ្រប់គ្រងកម្រៃជើងផ្សារ',
    'manage_zone'                 => 'គ្រប់គ្រងតំបន់',
    'manage_department'           => 'គ្រប់គ្រងផ្នែក',
    'manage_position'             => 'គ្រប់គ្រងតំណែង',
    'manage_users'                => 'គ្រប់គ្រងអ្នកប្រើប្រាស់',
    'manage_roles'                => 'គ្រប់គ្រងតួនាទី',
    'manage_permissions'          => 'គ្រប់គ្រងការអនុញ្ញាត',

    //detele
    'are_you_sure_do_you_want_to_delete' => 'តើអ្នកប្រាកដថាអ្នកចង់លុប?',
    'delete_information'                 => 'ព័ត៌មាន',
    'yes_delete'                         => 'បាទ / ចាស, លុប',
    'cancel'                             => 'បោះបង់',
    //Hr Module
    'hr_module'                  => 'គ្រប់គ្រងបុគ្គលិក',
    'personal_entry'             => 'ព័ត៌មានបុគ្កលិក',
    'contract_entry'             => 'ព័ត៌កុងត្រា',
    'list_all_employee'          => 'បង្ហាញព័ត៌មានបុគ្គលិកទាំងអស់',
    'addemployee'                => 'បន្ថែមព័ត៌មានបុគ្គលិក',
    'payroll'                    => 'ប្រាក់ខែ',
    'payroll_half'               => 'ប្រាក់ខែពាក់កណ្ដាល(Half Month)',
    'payroll_full'               => 'ប្រាក់ខែពេញ(Full Month)',
    //Personal entry
    'status'                     => 'ស្ថានភាព',
    'staff_phonenumber'          => 'លេខទូរស័ព្ទ',
    'place_Of_birth'             => 'ទីកន្លែងកំណើត',
    'date_of_birth'              => 'ថ្ងៃខែឆ្នាំកំណើត',
    'full_name_kh'               => 'ឈ្មោះជាភាសាខ្មែរ',
    'full_name_en'               => 'ឈ្មោះជាភាសាអង់គ្លេស',
    'staffId'                    => 'លេខសម្គាល់បុគ្គលិក',
    //Create Personal Entry
    'last_name_kh'               => 'នាមត្រកូលជាភាសាខ្មែរ',
    'first_name_kh'              => 'នាមខ្លួនជាភាសាខ្មែរ',
    'last_name_en'               => 'នាមត្រកូលជាភាសាអង់គ្លេស',
    'first_name_en'              => 'នាមខ្លួនជាភាសាអង់គ្លេស',
    'marital_status'             => 'ស្ថានភាពអាពាហ៍ពិពាហ៍',
    'national_id_card'           => 'លេខសម្គាល់អត្តសញ្ញាណប័ណ្ណជាតិ',
    'bankname'                   => 'ឈ្មោះធនាគារ',
    'bank_account_number'        => 'លេខគណនី',
    'driver_licence'             => 'ប័ណ្ណបើកបរ',
    'height'                     => 'កម្ពស់',
    'crrent_address'             => 'អាសយដ្ឋានបច្ចុប្បន្ន',
    'Province_town_city'         => 'ខេត្ត / ទីប្រជុំជន /ទីក្រុង',
    'district'                   => 'ស្រុក',
    'commune'                    => 'ឃុំ',
    'village'                    => 'ភូមិ',
    'village'                    => 'ភូមិ',
    'address_detail'             => 'ពត៌មានលំអិតអាសយដ្ឋាន',
    'emergency_contact'          => 'លេខ​ទំនាក់ទំនង​បន្ទាន់',
    'note'                       => 'ចំណាំ',
    'attachment_file'            => 'ឯកសារភ្ជាប់',
    'update'                     => 'កែប្រែ',
    'editemployee'               => 'កែប្រែព័ត៌មានបុគ្គលិក',
    // View Profile
    'view_profile'               => 'ប្រវត្តិរូប',
    'update_contract'            => 'កែប្រែកុងត្រា',
    'add_contract'               => 'បង្កើតកុងត្រាបុគ្គលិក',
    'about_me'                   => 'អំពីខ្ញុំ',
    'education'                  => 'ការអប់រំ',
    'skills'                     => 'ជំនាញ',
    'experience'                 => 'បទពិសោធន៍',
    'location'                   => 'ទីតាំង',
    'personal_data'              => 'ទិន្នន័យ​ផ្ទាល់ខ្លួន',
    'company_information'        => 'ព​ត៍​មាន​ក្រុមហ៊ុន',
    'bank_information'           => 'ព័ត៌មានធនាគារ',
    'current_address'            => 'អាស័យ​ដ្ឋាន​បច្ចុប្បន្',
    //Contract
    'date_of_employment'         => 'កាលបរិច្ឆេទបម្រើការងារ',
    'contract_type'              => 'ប្រភេទកិច្ចសន្យា',
    'company'                    => 'ឈ្មោះក្រុមហ៊ុន',
    'depart&branch'              => 'នាយកដ្ឋាន/សាខា',
    'position'                   => 'តួនាទី',
    'salary'                     => 'ប្រាក់ខែគោល',
    'currency'                   => 'រូបិយប័ណ្ណ',
    'contract_start_date'        => 'ថ្ងៃចុះកិច្ចសន្យា',
    'contract_end_date'          => 'ថ្ងៃបញ្ចប់កិច្ចសន្យា',
    'probation_end_date'         => 'ថ្ងៃបញ្ចប់ការសាកល្បង',
    'pay_tax_by_company'         => 'បង់ពន្ធដោយក្រុមហ៊ុន',
    'transfer_work_to'           => 'ផ្ទេរការទៅ',
    'get_work_from'              => 'ទទួលការងារពី',
    'reason'                     => 'មូលហេតុ',
    'contract'                   => 'បញ្ជីរកុងត្រា',
    'list_all_contract'          => 'បង្ហាញព័ត៌មានកុងត្រាទាំងអស់',
    'addcontract'                => 'បង្កើតកុងត្រា',
    'modifycontract'             => 'កែប្រែព័ត៌មានកុងត្រា',
    'staffname'                  => 'ឈ្មោះបុគ្គលិក',
    //operation
    'operation'                  => 'ប្រតិបត្តិការប្រចាំថ្ងៃ',

    //confirm payment with staff
    'confirm_payment'            => 'បញ្ជាក់ការទូទាត់ប្រាក់',
    'filter_by_delivery_today'   => 'ជ្រើសរើសអ្នកដឹកជញ្ជូនសម្រាប់ថ្ងៃនេះ',
    'verify_by'                  => 'ផ្ទៀងផ្ទាត់ដោយ',

    //currency
    'list_all_currency'            => 'បង្ហាញរូបិយប័ណ្ណទាំងអស់',
    'currency'                     => 'រូបិយប័ណ្ណ',
    'usd_name'                     => 'ឈ្មោះដុល្លារអាមេរិក',
    'usd_amount'                   => 'ចំនួនដុល្លារអាមេរិក',
    'kh_name'                      => 'ឈ្មោះប្រាក់រៀល',
    'kh_amount'                    => 'ចំនួនប្រាក់រៀល',
    'symbol'                       => 'និមិត្តសញ្ញា',
    'please_check_amount'          => 'សូមពិនិត្យមើលទឹកប្រាក់ឡើងវិញម្ដងទៀត($/៛)',
    'amount'                       => 'ចំនួន',

  //income expense
  'list_all_income_expense'      => 'បង្ហាញចំណូល&ចំណាយទាំងអស់',
  'types'                        => 'ប្រភេទ',
  'payment_methods'              => 'វិធីសាស្រ្តទូទាត់ប្រាក់',
  'transaction_date'             => 'ថ្ងៃ​ធ្វើ​ប្រតិបត្តិការ',
  'receipt'                      => 'បង្កាន់ដៃ',
  'methods'                      => 'ការទូទាត់',
  'us'                           => 'ប្រាក់ដុល្លា',
  'riels'                        => 'ប្រាក់រៀល',
  'total_balance'                => 'សរុបសមតុល្យប្រាក់',
  'transaction'                  => 'ប្រតិបត្តិការ',
  'bank'                         => 'ធនាគា',
  'cash'                         => 'សាច់ប្រាក់',
  'deliver'                      => 'ចំនួនដឹក',
  'review'                       => 'ពិនិត្យ',

  //payroll
  'card'                            => 'ថ្លៃកាតទូរស័ព្ទ',
  'gasoline'                        => 'ថ្លៃសាំង',
  'list_all_payroll'                => 'បញ្ជីប្រាក់ខែទាំងអស់',
  'payroll'                         => 'ប្រាក់ខែ',
  'add_payroll'                     => 'បន្ថែមប្រាក់ឈ្នួល',

  //payslip
  'total_salary'                      => 'ប្រាក់ខែសរុប',
  'staff_name'                        => 'ឈ្មោះបុគ្គលិក',
  'tax'                               => 'កាត់ពន្ធ',
  'company_deduction'                 => 'ប្រាក់ខ្ចីក្រុមហ៊ុន',
  'bonus'                             => 'ប្រាក់លើកទឹកចិត្ត',
  'increase_salary'                   => 'ដំឡើងប្រាក់ខែ',
  'basic_salary'                      => 'ប្រាក់ខែ​គោល',
   //ផ្ទាំងគ្រប់គ្រង
   'dashboard'                                    => 'ផ្ទាំងគ្រប់គ្រង',
   'total'                                        => 'សរុប',
   'average'                                      => 'ជាមធ្យម',
   'days'                                         => 'ថ្ងៃ',
   'capital_amount'                               => 'ចំនួនប្រាក់សរុបទាំងអស់',
   'daily_revenue'                                => 'ចំណូលប្រចាំថ្ងៃ',
   'total_amount_in_bank'                         => 'ចំនួនទឹកប្រាក់សរុបនៅក្នុងធនាគារ',
   'total_amount_in_cash'                         => 'ចំនួនទឹកប្រាក់សរុបជាសាច់ប្រាក់',
   'transaction_and_delivery_income_by_shop'      => 'ចំណូលប្រតិបត្តិការនិងការដឹកជញ្ជូនតាមហាង',
   'transaction_and_delivery_income_by_staff'     => 'ចំណូលប្រតិបត្តិការនិងការដឹកជញ្ជូនដោយបុគ្គលិក',
   'delivery_fee'                                 => 'ថ្លៃសេវាដឹកជញ្ជូន',
   'reject'                                       => 'បដិសេធ',
   'deduction_name'                               => 'បុគ្គលិកដែរបានខ្ចីប្រាក់ក្រុមហ៊ុនមុន',
   'please_check_the_total_amount'                => 'សូមពិនិត្យចំនួនប្រាក់សរុបឡើងវិញមិនស្មើទេ',
   'page'                                         => 'គ្រប់គ្រងគេហទំព័រ',

   'domain_&_hosting'                             => 'ខ្មែរ',
   'incomes_&_expenses'                               => 'ចំណូល&ចំណាយ',
];
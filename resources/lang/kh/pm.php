<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home'          => 'ទំព័រដើម',
    'listing'       => 'តារាង (អចលនទ្រព្យ)',
    'property'      => 'អចលនទ្រព្យ',
    'for_sale'      => 'សម្រាប់​លក់',
    'for_rent'      => 'សម្រាប់​ជួល',
    'news'          => 'ព័ត័មាន',
    'gallery'       => 'Album រូបភាព',
    'contact_us'    => 'ទំនាក់ទំនងមកយើងខ្ញុំ',



    'location'             => 'ទីតាំង',
    'property_description' => 'ការពិពណ៍នា​អចលនទ្រព្យ',
    'quick_summary'        => 'ពត៌មានសង្ខេប',
    'sale_price'           => 'តម្លៃលក់',
    'land_size'            => 'ទំហំដី',
    'building_size'        => 'ទំហំអាគារ',
    'types'                => 'ប្រភេទ',
    'status'               => 'អំពីស្ថានភាព',
    'property_types'       => 'បប្រភេទអចលនទ្រព្យ',
    'bedroom'              => 'បន្ទប់គេង',
    'bathroom'             => 'បន្ទប់ទឹក',
    'parking'              => 'ចំណត',
    'living_room'          => 'បន្ទប់ទទួលភ្ញៀវ',
    'legal_document'       => 'ឯកសារច្បាប់',
    'property_owner'       => 'ម្ចាស់អចលនទ្រព្យ',
    'general_amenities'    => 'គ្រឿងបរិក្ខារទូទៅ',
    'search'               => 'ស្វែងរក',


    'search_properties'        => 'ស្វែងរកអចលនទ្រព្យ',
    'all_actions'              => 'ប្រភេទទាំងអស់',
    'all_property_types'       => 'ប្រភេទអចលនទ្រព្យទាំងអស់',
    'all_location'             => 'ទីតាំងទាំងអស់',
    'all_status'               => 'ស្ថានភាពទាំងអស់',
    'latest_in_sales'          => 'ការដាក់លក់ចុងក្រោយ',
    'view_more'                => 'មើលបន្ថែម',
    'property_highlights'      => 'ការតាំងបង្ហាញអចលនទ្រព្យ',
    'latest_listings'          => 'បង្ហាញអចលនទ្រព្យចុងក្រោយ',
    'meet_our_agent'           => 'ជាប់ជាមួយភ្នាក់ងារលក់',
    'from_the_blog'            => 'ព័ត៍មានថ្មីៗ',
    'contact_info'             => 'ព័ត៌មានទំនាក់ទំនង',
    'image_gallery'            => 'កម្រងរូបភាព',
    'user_full_links'          => 'តំណភ្ចាប់ទៅកាន់អចលនទ្រព្យ',
    'all_rights'               => 'រក្សាសិទ្ធិគ្រប់យ៉ាងដោយ​ P.M Family & Investment - បង្កើតឡើងដោយ ',
    'advanced_search'          => 'ការស្វែងរកកម្រិតខ្ពស់',
    'search_now'               => 'ស្វែងរកឥឡូវនេះ',
    'featured_properties'      => 'លក្ខណៈអចលនទ្រព្យ',
    'sort_by'                  => 'តម្រៀបតាម',
    'newest_first'             => 'ថ្មីបំផុត',
    'lowest_price'             => 'តម្លៃ​ទាប​បំផុត',
    'highest_price'            => 'តម្លៃខ្ពស់បំផុត',
    'date_added'               => 'កាលបរិច្ឆេទ​ដែល​បាន​បន្ថែម',
    'details'                  => 'ព័ត៌មានលម្អិត',
    'our_gallery'              => 'កម្រងរូបភាពរបស់យើង',
    'book_by_phone'            => 'កក់តាមទូរស័ព្ទ',
    'location_address'         => 'អាសយដ្ឋាន/ទីតាំង',
    'give_your_feedback'       => 'ផ្តល់យោបល់របស់អ្នក។',
    'send_us_a_message'        => 'ផ្ញើសារមកយើង',
    'send_message'             => 'ផ្ញើ​សារ',
    'login'                    => 'ចូលប្រើប្រាស់ប្រព័ន្ធ',
    'property_listing'         => 'បង្ហាញអចលនទ្រព្យទាំងអស់',

    'agent_details'            => 'ព័ត៍មានលម្អិតអំពីភ្នាក់ងារលក់',
    'categories'               => 'ប្រភេទអចលនទ្រព្យ',
    'latest_news'              => 'ព័ត៍មានចុងក្រោយ',
    'location_on_map'          => 'ទីតាំង Google Map',
    'similar_properties'       => 'អចលនទ្រព្យស្រដៀងៗគ្នា',
    'read_more'                => 'អានបន្ថែម',
    'news_details'             => 'ព័ត៍មានលម្អិត',
    'all'                      => 'ទាំងអស់',

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home'          => 'Home',
    'listing'       => 'Listing',
    'property'      => 'Property',
    'for_sale'      => 'For Sale',
    'for_rent'      => 'For Rent',
    'news'          => 'News',
    'gallery'       => 'Gallery',
    'contact_us'    => 'Contact Us',


    'location'             => 'Location',
    'property_description' => 'Property Description',
    'quick_summary'        => 'Quick Summary',
    'sale_price'           => 'Sale Price',
    'land_size'            => 'Land Size',
    'building_size'        => 'Building Size',
    'types'                => 'Types',
    'status'               => 'Status',
    'property_types'       => 'Property Types',
    'bedroom'              => 'Bedroom',
    'bathroom'             => 'Bathroom',
    'parking'              => 'Parking',
    'living_room'          => 'Living Room',
    'legal_document'       => 'Legal Document',
    'property_owner'       => 'Property Owner',
    'general_amenities'    => 'General Amenities',
    'search'               => 'Search',


    'search_properties'        => 'SEARCH PROPERTIES',
    'all_actions'              => 'All Actions',
    'all_property_types'       => 'All Property Types',
    'all_location'             => 'All Location',
    'all_status'               => 'All Status',
    'latest_in_sales'          => 'LATEST IN SALES',
    'view_more'                => 'View More',
    'property_highlights'      => 'Property Highlights',
    'latest_listings'          => 'LATEST LISTINGS',
    'meet_our_agent'           => 'MEET OUR AGENT',
    'from_the_blog'            => 'FROM THE BLOG',
    'contact_info'             => 'CONTACT INFO',
    'image_gallery'            => 'IMAGE GALLERY',
    'user_full_links'          => 'USEFULL LINKS',
    'all_rights'               => 'All rights reserved P.M Family & Investment  - Developed by ',
    'advanced_search'          => 'Advanced Search',
    'search_now'               => 'Search Now',
    'featured_properties'      => 'Featured Properties',
    'sort_by'                  => 'Sort By',
    'newest_first'             => 'Newest',
    'lowest_price'             => 'Lowest Price',
    'highest_price'            => 'Highest Price',
    'date_added'               => 'Date added',
    'details'                  => 'Details',
    'our_gallery'              => 'OUR GALLERY',
    'book_by_phone'            => 'Book By Phone',
    'location_address'         => 'Location Address',
    'give_your_feedback'       => 'Give Your Feedback',
    'send_us_a_message'        => 'SEND US A MESSAGE',
    'send_message'             => 'Send Message',
    'login'                    => 'Login',
    'property_listing'         => 'Property Listing',

    'agent_details'            => 'Agent Details',
    'categories'               => 'Categories',
    'latest_news'              => 'Latest News',
    'location_on_map'          => 'Location On Map',
    'similar_properties'       => 'Similar Properties',
    'read_more'                => 'Read More',
    'news_details'             => 'News Details',
    'all'                      => 'All',






];

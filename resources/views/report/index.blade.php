@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item active">@lang('message.report')</li>
        </ol>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.income_&_expense')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-3">
                                <div class="form-group">
                                    <div class="controls"  id="data_3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                                <input class="form-control input-group date current_date start_date" autocomplete="off" name="date" type="text" placeholder="Start Date">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-3">
                                <div class="form-group">
                                    <div class="controls"  id="data_3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                                <input class="form-control input-group date current_date end_date" autocomplete="off" name="date" type="text" placeholder="End Date">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-lg-3">
                                <div class="form-group">
                                    <div class="controls" >
                                        <select name="type"  class="form-control type" style="width: 100% !important;">
                                                <option value="0">--ប្រភេទ--</option>
                                        
                                                @foreach($type as $k=>$v){
                                                    <option value="{{$v->id}}">{{$v->name_en}}</option>
                                                }@endforeach

                                                
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-3">
                                <div class="form-group">
                                    <div class="controls">
                                        <select name="method"  class="form-control method" style="width: 100% !important;">
                                                <option value="0">--ការទូទាត់--</option>
                                                @foreach($method as $k=>$v){
                                                    <option value="{{$v->id}}">{{$v->name_en}}</option>
                                                }@endforeach
                                               
                                            </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-responsive-sm" id="data-user">
                            <thead>
                                <tr>
                                    <th>ល.រ</th>
                                    <th>ការទូទាត់</th>
                                    <th>បង្កាន់ដៃ</th>
                                    <th>ប្រភេទ</th>
                                    <th>ប្រាក់ដុល្លា($)</th>
                                    <th>ប្រាក់រៀល(៛)</th>
                                    <th>បញ្ចូលដោយ</th>
                                    <th>កាលបរិច្ឆេទ</th>
                                    <th>បរិយាយ</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    <script type="text/javascript">

        var url = "{{route('report.getReportList')}}";
        var dataUser = '';
        $(document).ready(function() {
            dataUser =$('#data-user').DataTable({
                "language": {

                    "search": "@lang('message.search')",
                    "paginate": {
                        "previous": "@lang('message.previous')",
                        "next": "@lang('message.next')"
                    }
                },
                "processing": true,
                "serverSide": true,
                // "retrieve": true,
                "bLengthMenu" : true, //thought this line could hide the LengthMenu
                "bInfo":true,
                //"length":10,
                "pageLength": 10,
                "autoWidth": false,
                "scrollX": true,
                "ajax": url,
                "columns": [
                    {
                        "render": function (data, type, aa){
                            return aa.DT_RowIndex;
                        },
                    },
                    {data: 'receipt',name: 'receipt'},
                    {
                        "render": function (data, type, aa){
                            return aa.get_method.name_en;
                        },
                    },
                    {
                        "render": function (data, type, aa){
                            return aa.get_type.name_en;
                        },
                    },
                  
                    
                    {data: 'amount_usd',name: 'amount_usd'},
                    {data: 'amount_khr',name: 'amount_khr'},
                    
                    {
                        "render": function (data, type, aa){
                            return aa.get_response.name;
                        },
                    },
                    
                    {data: 'date',name: 'date'},
                    {data: 'description',name: 'description'},
                    
                ],
                'columnDefs': [
                    {targets: [0],"className": "text-center"},
                  //   { targets: '_all', "className": "small " },
                    { "orderable": true},
                ],
            });
    });


    $('.start_date, .end_date, .type, .method').change( function () {
          
            var start_date     = $('.start_date').val();
            var end_date       = $('.end_date').val();
            var method         = $('.method').val();
            var type           = $('.type').val();
            
            dataUser.clear().draw();
            dataUser.ajax.url(url+ "?start_date=" + start_date + "&end_date=" +end_date + "&method=" +method + "&type=" +type).load();
        });
    $('#data_3 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            format: "yyyy/mm/dd",
            showButtonPanel: true,
            gotoCurrent: true,
        });
   
    </script>
@endsection

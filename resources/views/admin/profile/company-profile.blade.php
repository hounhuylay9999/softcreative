@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item active">@lang('message.profile')</li>
        </ol>
        
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($profile, ['route' => array('profile.companyProfileUpdate',$profile->id),'enctype'=>'multipart/form-data','id'=>'ImageForm','class'=>'form-horizontal']) !!}
                    <div class="card-header">@lang('message.profile')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-4 mb-4">
                                <div class="text-center">


                                    <div class="file-box">
                                        <div class="file">
                                           
                                                <span class="corner"></span>
                
                                                <div class="">
                                                    <div class="m-b-sm">
                                                        <span class="profile-picture">
                                                            <div class="logo-thumnail">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-news img-circle">
                                                                        <img class="editable img-circle" src="@if(isset($profile->profile) == null || $profile->profile == ''){{ url('/upload/delivery.png') }}@else{{ asset('upload/'.$profile->profile) }}@endif" id="image-defalt" class="img-circle1">
                                                                    </div>
                                                                    <div class="mt-3">
                                                                        <span class="btn btn-square btn-info btn-file">
                                                                            <span class="fileinput-exists"> @lang('message.change_logo') </span>
                                                                            {{ Form::file('profile',['id'=>'images','accept'=>'image/*']) }}
                    
                                                                        </span>
                                                                    </div>
                                                                    <div id="image_message"></div>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                 
                                                </div>
                                                <div class="file-name">
                                                    {{ $profile->name }}
                                                    <br>
                                                    <small>{{ $profile->created_at->format('M d, Y') }}</small>
                                                </div>
                                            
                                        </div>
                                    </div>
    
    
                                </div>
                            </div>
                            
                            <div class="col-md-8 mb-4">

                                <div class="row">
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.company_name_en')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('name_en')) has-error @endif">
                                                {!! Form::text('name_en', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('name_en'))
                                                    <span class="text-danger">{{$errors->first('name_en')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.company_name_kh')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('name_kh')) has-error @endif">
                                                {!! Form::text('name_kh', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('name_kh'))
                                                    <span class="text-danger">{{$errors->first('name_kh')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.company_email')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('email')) has-error @endif">
                                                {!! Form::text('email', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('email'))
                                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.company_phone')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('phone')) has-error @endif">
                                                {!! Form::text('phone', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('phone'))
                                                    <span class="text-danger">{{$errors->first('phone')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group row">
                                            <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.company_address')</label> <span class="text-danger">*</span></div>
                                            <div class="col-sm-12 @if($errors->has('address')) has-error @endif">
                                                {!! Form::text('address', null, array('placeholder' => '','class' => 'form-control')) !!}
                                                @if($errors->first('address'))
                                                    <span class="text-danger">{{$errors->first('address')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"  id="submit1"> @lang('message.modify')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        
        <style>
        .editable{
            width:120px;
            height:120px;
            object-fit:cover;
            border-radius:50%;
        }
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .nav-tabs{margin-bottom: 20px !important;}
</style>

@endsection


@section('scripts')
    <script type="text/javascript">
        $("#images").change(function(e) {
            e.preventDefault();
            $("#image_message").html(''); // To remove the previous error message
            var file = this.files[0];
            defaultimg = file.name;
            var imagefile = file.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            var sizefile = this.files[0].size;
            var form = this;
            file_check = this.files && this.files[0];

            var fileName = document.getElementById("images").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

            console.log(extFile);

            //check size
            if (sizefile < 9000000) {
                //check width and height
                if( file_check ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                        window.URL.revokeObjectURL( img.src );

                        if( (width <= 1024 && height <= 1024) && ((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                            $("#image_message").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                            var reader = new FileReader();
                            //imageIsLoaded
                            reader.onload = function(e) {
                                $("#images").css("color", "green");
                                document.getElementById("submit1").disabled = false;
                                $('#image-defalt').attr('src', e.target.result);
                            };
                            reader.readAsDataURL(file);

                        }
                        else {
                            document.getElementById("submit1").disabled = true;
                            $('#image-defalt').attr('src', '{{asset('/')}}upload/delivery.png');
                            $("#image_message").html("<p class='text-danger'>Your image allow min size(1024x1024)pixcel <br> Allow File Type: jpeg,png,jpg</p>");
                            return false;
                        }
                    }
                }else {
                    $('#image-defalt').attr('src', '{{asset('/')}}upload/delivery.png');
                    $('.fileinput-exists').attr('src', '{{asset('/')}}upload/delivery.png');
                    document.getElementById("submit1").disabled = true;
                    $("#image_message").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                    return false;
                }

            } else {
                document.getElementById("submit1").disabled = true;
                $('#image-defalt').attr('src', '{{asset('/')}}upload/delivery.png');
                $("#image_message").html("<p class='text-danger'>Your image more then 1M</p>");
                return false;
            }
        });

    
    </script>
@endsection

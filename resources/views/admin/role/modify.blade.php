@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}"> @lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('roles.index') }}"> @lang('message.roles')</a></li>
            <li class="breadcrumb-item active"> @lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('roles.index') }}"> @lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', \Crypt::encrypt($role->id)],'class'=>'form-horizontal']) !!}
                    <div class="card-header"> @lang('message.add_new')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email"> @lang('message.roles')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('roles')) has-error @endif">
                                        {!! Form::text('roles', $role->name, array('placeholder' => 'Please enter roles','class' => 'form-control role_name')) !!}
                                        @if($errors->first('roles'))
                                            <span class="text-danger">{{$errors->first('roles')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email"> @lang('message.slug')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('slug')) has-error @endif">
                                        {!! Form::email('slug', $role->slug, ['placeholder' => '','class' => 'form-control slug','readonly'=>true]) !!}
                                        @if($errors->first('slug'))
                                            <span class="text-danger">{{$errors->first('slug')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="row">
                            <div class="col-md-12 col-lg">
                                <div class="nav-tabs-boxed nav-tabs-boxed-left">
                                    <div class="row">
                                        <div class="col-md-6 text-right">
                                            <ul class="nav nav-tabs" role="tablist">
                                                @foreach($permission as $keyp => $value)
                                                    <li class="nav-item"><a class="nav-link @if($keyp==0) active @endif" data-toggle="tab" href="#home-{{$keyp}}" role="tab" aria-controls="home-{{$keyp}}" aria-selected="true">{{$value->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="tab-content">
                                                @foreach($permission as $key => $value)
                                                <div class="tab-pane @if($key==0) active @endif" id="home-{{$key}}" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-sm-12 label-role">
                                                            <label>
                                                                <input type="checkbox" class="checkPermission" cindex="{{$key}}">
                                                                <strong>Assign all permission</strong>
                                                            </label>
                                                        </div>
                                                    </div>
        
                                                    <div class="row">
                                                        <div class="col-md-12 checkpermissionbypermission_{{$key}}">
                                                            @foreach($sub_group as $k => $b)
                                                                @if($value->id == $b->menu_permission_id)
                                                                    <label class="text-danger" style="width: 100%">
                                                                        <strong>{{$b->name}}</strong>
                                                                    </label>
                                                                    <div class="hr-line-dashed"></div>
                                                                    <div class="row">
                                                                    @foreach($value->getPermissionByGroup as $index => $v)
                                                                        @if($v->menu_sub_group_id == $b->id)
                                                                        <div class="col-sm-6">
                                                                            <label class="form-group">
                                                                                
                                                                                {{ Form::checkbox('permission[]', $v->id, in_array($v->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                                                               
                                                                                {{ $v->name_en }}</label>
                                                                            <br/>
                                                                        </div>
                                                                        @endif
                                                                    @endforeach
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
        
        
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12  @if($errors->has('permission')) has-error @endif">
                            @if($errors->first('permission'))
                                <span class="text-danger">{{$errors->first('permission')}}</span>
                            @endif
                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.modify')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <style>
          .nav-tabs-boxed.nav-tabs-boxed-left .nav-item{float: left; width: 100%}
        .nav-tabs-boxed.nav-tabs-boxed-left, .nav-tabs-boxed.nav-tabs-boxed-right{
            display: inherit !important;
            width: 100%;
        }
        .nav-tabs-boxed.nav-tabs-boxed-left .nav-link:hover,
        .nav-tabs-boxed.nav-tabs-boxed-left .nav-link.active{
            border: 0px !important;
            color: #e55353 !important
        }
        .nav-tabs-boxed.nav-tabs-boxed-left .nav-link{color: #333;}
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">
       
        $(".checkPermission").click(function () {
            var cindex = $(this).attr('cindex');
            $(".checkpermissionbypermission_"+cindex+" input:checkbox").not(this).prop('checked', this.checked);
        });

        $(".role_name").keyup(function(){
            var Text = $(this).val();
            Text = Text.toLowerCase();
            Text = Text.replace('/\s/g','-');
            Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
            $(".slug").val(Text);    
        });
    </script>
@endsection

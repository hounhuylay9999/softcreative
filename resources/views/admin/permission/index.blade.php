@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item active">@lang('message.manage_permissions')</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.manage_permissions')</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm" id="data-role">
                            <thead>
                                <tr>
                                    <th>@lang('message.no')</th>
                                    <th>Name En</th>
                                    <th>Name Kh</th>
                                    <th>Slug</th>
                                    <th>@lang('message.date')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script type="text/javascript">

        var url = "{{route('permission.getPermissionList')}}";
        $(document).ready(function() {
            var dataUser =$('#data-role').DataTable({
                "language": {
                    "lengthMenu": "@lang('message.display') _MENU_ @lang('message.records_per_page')",
                    "zeroRecords": "@lang('message.no_matching_records_found')",
                    "info": "@lang('message.showing_page') _PAGE_ @lang('message.of') _PAGES_",
                    "infoEmpty": "@lang('message.no_records_available')",
                    "infoFiltered": "(@lang('message.filtered_from') _MAX_ @lang('message.total_records'))",
                    "search": "@lang('message.search')",
                    "paginate": {
                        "previous": "@lang('message.previous')",
                        "next": "@lang('message.next')"
                    }
                },
                "processing": true,
                "serverSide": true,
                // "retrieve": true,
                "bLengthMenu" : true, //thought this line could hide the LengthMenu
                "bInfo":true,
                //"length":10,
                "pageLength": 10,
                "autoWidth": false,
                "scrollX": true,
                "ajax": url,
                "columns": [
                    {
                        "render": function (data, type, aa){
                            return aa.DT_RowIndex;
                        },
                    },
                    {data: 'name_en',name: 'name_en'},
                    {data: 'name_kh',name: 'name_kh'},
                    {data: 'slug',name: 'slug'},
                    {data: 'date',name: 'date'},
                ],
                'columnDefs': [
                    {targets: [0],"className": "text-center"},
                  //   { targets: '_all', "className": "small " },
                    { "orderable": true},
                ],
            });
        });

    </script>
@endsection

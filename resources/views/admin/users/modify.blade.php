@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">@lang('message.users')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('users.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}

        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.full_name')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('name')) has-error @endif">
                                        {!! Form::text('name', null, array('placeholder' => 'Please enter fullname','class' => 'form-control')) !!}
                                        @if($errors->first('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.email')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('email')) has-error @endif">
                                        {!! Form::email('email', null, ['placeholder' => 'Please enter email','class' => 'form-control']) !!}
                                        @if($errors->first('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.roles')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('roles')) has-error @endif">
                                        <select name="roles[]"  class="form-control select2" style="width: 100% !important;">
                                            @foreach($roles as $key => $v)
                                                <option @if($v->name == $userRole) selected @endif value="{{ $v->name }}">{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('roles'))
                                            <span class="text-danger">{{$errors->first('roles')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Position</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('position')) has-error @endif">
                                        {!! Form::text('position', null, ['placeholder' => 'Position','class' => 'form-control']) !!}
                                        @if($errors->first('position'))
                                            <span class="text-danger">{{$errors->first('position')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Phone Number <sup>1</sup></label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('phone_number')) has-error @endif">
                                        {!! Form::text('phone_number', null, ['placeholder' => 'Phone Number','class' => 'form-control']) !!}
                                        @if($errors->first('phone_number'))
                                            <span class="text-danger">{{$errors->first('phone_number')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Phone Number <sup>2</sup></label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('fax')) has-error @endif">
                                        {!! Form::text('fax', null, ['placeholder' => 'Phone Number','class' => 'form-control']) !!}
                                        @if($errors->first('fax'))
                                            <span class="text-danger">{{$errors->first('fax')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label class="col-md-12 col-form-label">@lang('message.gender') <span class="text-danger">*</span></label>
                                    <div class="col-md-12 col-form-label">
                                        @foreach($gender as $k => $v)
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" id="inline-radio1" type="radio" value="{{ $v->id }}" name="gender" @if($user->gender == $v->id) checked @endif  {{ old('gender') == $v->id ? 'checked' : '' }} >
                                            <label class="form-check-label" for="inline-radio1">{{ $v->name_en }}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.password')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('password')) has-error @endif">
                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                        @if($errors->first('password'))
                                            <span class="text-danger">{{$errors->first('password')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">@lang('message.confirm_password')</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('confirm-password')) has-error @endif">
                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                        @if($errors->first('confirm-password'))
                                            <span class="text-danger">{{$errors->first('confirm-password')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.modify')</button>
                    </div>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="text-center">
                        <div class="file-box">
                            <div class="file">
                                <span class="corner"></span>
                                <div class="p-3">
                                    <div class="m-b-sm">
                                        <span class="profile-picture">
                                            <div class="logo-thumnail">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-news img-circle">
                                                        <img class="img-circle img-circle1 image-defalt-news_cover" src="{{ asset('upload/'.$user->profile) }}">
                                                    </div>
                                                    <div class="mt-3">
                                                        <span class="btn btn-square btn-info btn-file">
                                                            <span class="fileinput-exists"> Thumbnail (960x718) Pixcel</span>
                                                            {{ Form::file('thumbnail',['class'=>'news_cover','accept'=>'image/*']) }}
                                                        </span>
                                                    </div>
                                                    <div class="image_message_news_cover"></div>
                                                    @if($errors->first('thumbnail'))
                                                            <span class="text-danger">{{$errors->first('thumbnail')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </span>
                                    </div>

                                </div>
                                <div class="file-name"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }

        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-tabs .nav-link{
            border: 0px solid #dadada;
            border-radius: 0px;
        }
    </style>

@endsection


@section('scripts')
<script type="text/javascript">
    $(".news_cover").change(function(e) {
       e.preventDefault();
       $(".image_message_news_cover").html(''); // To remove the previous error message
       var file = this.files[0];
       defaultimg = file.name;
       var imagefile = file.type;
       var match = ["image/jpeg", "image/png", "image/jpg"];
       var sizefile = this.files[0].size;
       var form = this;
       file_check = this.files && this.files[0];

       var fileName = $('.news_cover').val();
       var idxDot = fileName.lastIndexOf(".") + 1;
       var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

       //console.log(extFile);

       //check size
       if (sizefile < 9000000) {
           //check width and height
           if( file_check ) {
               var img = new Image();
               img.src = window.URL.createObjectURL( file );
               img.onload = function() {
                   var width = img.naturalWidth,
                       height = img.naturalHeight;
                   window.URL.revokeObjectURL( img.src );

                   if(((extFile == 'png') || (extFile == 'jpg') || (extFile == 'jpeg'))) {
                       $(".image_message_news_cover").html("<p class='text-success'>ជោគជ័យសម្រាប់រូបភាពដែលជ្រើសរើស</p>");
                       var reader = new FileReader();
                       //imageIsLoaded
                       reader.onload = function(e) {
                           $(".news_cover").css("color", "green");
                         //  document.getElementById("submit1").disabled = false;
                           $('.image-defalt-news_cover').attr('src', e.target.result);
                       };
                       reader.readAsDataURL(file);

                   }
                   else {
                      // document.getElementById("submit1").disabled = true;
                       $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
                       $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
                       return false;
                   }
               }
           }else {
               $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
               $('.fileinput-exists').attr('src', '{{asset('/')}}assets/img/no_img.png');
               //document.getElementById("submit1").disabled = true;
               $(".image_message_news_cover").html("<p class='text-danger'>Allow File Type: jpeg,png,jpg</p>");
               return false;
           }

       } else {
           //document.getElementById("submit1").disabled = true;
           $('.image-defalt-news_cover').attr('src', '{{asset('/')}}assets/img/no_img.png');
           $(".image_message_news_cover").html("<p class='text-danger'>Your image more then 1M</p>");
           return false;
       }
   });

   </script>
@endsection

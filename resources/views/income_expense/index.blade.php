@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item active">@lang('message.income_&_expense')</li>
        </ol>

        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('income_expense.create') }}">@lang('message.add_new')</a>
        </div>

    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.list_all_income_income_&_expense')</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm" id="data-user">
                            <thead>
                                <tr>
                                    <th>@lang('message.no')</th>
                                    <th>Receipt</th>
                                    <th>Method</th>
                                    <th>Types</th>
                                    <th>Us($)</th>
                                    <th>Type(៛)</th>
                                    <th>Entry By</th>
                                    <th>Description</th>
                                    <th>@lang('message.date')</th>
                                    <th>@lang('message.action')</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog">
            <form action="" id="deleteForm" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('message.delete_informaton')</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <p class="text-center">@lang('message.are_you_sure_do_you_want_to_delete')</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-square" data-dismiss="modal">@lang('message.cancel')</button>
                        <button type="submit"  class="btn btn-danger btn-square" data-dismiss="modal" onclick="formSubmit()">@lang('message.yes_delete')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection


@section('scripts')

    <script type="text/javascript">

        var url = "{{route('income_expense.getIncomeExpenseList')}}";
        $(document).ready(function() {
            var dataUser =$('#data-user').DataTable({
                "language": {

                    "search": "@lang('message.search')",
                    "paginate": {
                        "previous": "@lang('message.previous')",
                        "next": "@lang('message.next')"
                    }
                },
                "processing": true,
                "serverSide": true,
                // "retrieve": true,
                "bLengthMenu" : true, //thought this line could hide the LengthMenu
                "bInfo":true,
                //"length":10,
                "pageLength": 10,
                "autoWidth": false,
                "scrollX": true,
                "ajax": url,
                "columns": [
                    {
                        "render": function (data, type, aa){
                            return aa.DT_RowIndex;
                        },
                    },
                    {data: 'receipt',name: 'receipt'},
                    {data: 'name_en',name: 'name_en'},
                    {data: 'type_name',name: 'type_name'},
                    {data: 'amount_usd',name: 'amount_usd'},
                    {data: 'amount_khr',name: 'amount_khr'},
                    
                    {
                        "render": function (data, type, aa){
                            return aa.get_response.name;
                        },
                    },
                    {data: 'description',name: 'description'},
                    {data: 'date',name: 'date'},
                    {data: 'action',name: 'action'},
                ],
                'columnDefs': [
                    {targets: [0],"className": "text-center"},
                  //   { targets: '_all', "className": "small " },
                    { "orderable": true},
                ],
            });

//            setInterval( function () {
//                dataUser.ajax.reload();
//            }, 18000)
        });







        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("income_expense.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
@endsection

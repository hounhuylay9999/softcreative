
@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('income_expense.index') }}">@lang('message.income_&_expense')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('income_expense.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::model($income, ['method' => 'PATCH','route' => ['income_expense.update', $income->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.income_&_expense')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Receipt</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('receipt')) has-error @endif">
                                        {!! Form::text('receipt', null, array('placeholder' => 'Please enter receipt','class' => 'form-control')) !!}
                                        @if($errors->first('receipt'))
                                            <span class="text-danger">{{$errors->first('receipt')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">USD Amount($)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('usd')) has-error @endif">
                                    <!-- {!! Form::text('amount_usd', null, array('placeholder' => 'Please enter USD Amount($)','class' => 'form-control')) !!} -->
                                    <!-- {!! Form::text('amount_usd') !!} -->
                                    <div class="input-group mb-3" style="margin-bottom: 0rem !important;">
                                        <span class="input-group-text" style="border-radius: 0;">$</span>
                                        <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="usd" value="{{$income->amount_usd}}">
                                        <span class="input-group-text" style="border-radius: 0;">.00</span>
                                        
                                    </div>
                                    
                                    
                                    @if($errors->first('usd'))
                                                <span class="text-danger">{{$errors->first('usd')}}</span>
                                    @endif
                                    <!-- {{Form::date('date', \Carbon\Carbon::now())}} -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Riel Amount(៛)</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('riel')) has-error @endif">
                                    <div class="input-group mb-3" style="margin-bottom: 0rem !important;">
                                        <span class="input-group-text" style="border-radius: 0;">៛</span>
                                        <input type="text" class="form-control" aria-label="Amount (to the nearest real)" name="riel" value="{{$income->amount_khr}}">
                                        <span class="input-group-text" style="border-radius: 0;">.00</span>
                                    </div>
                                        @if($errors->first('riel'))
                                            <span class="text-danger">{{$errors->first('riel')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row" style="text-align: right; float:right">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Types</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('type')) has-error @endif">

                                    @foreach($type as $key=>$v)
                                        
                                        <input type="radio"  @if($income->type_id==$v->id) checked @endif name="type_id" value="{{$v->id}}" >
                                        
                                        <label for="nf-email">{{ $v->name_en }}</label>&nbsp;&nbsp;&nbsp;
                                        
                                    @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Transaction</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('date')) has-error @endif">
                                    <div class="controls"  id="data_3">
                                        <!-- {{!! form::date("date") !!}} -->
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                                <input class="form-control input-group date current_date" autocomplete="off" name="date" type="text" placeholder="yyyy/mm/dd" value="{{$income->date}}">
                                            </div>
                                        </div>
                                        @if($errors->first('date'))
                                            <span class="text-danger">{{$errors->first('date')}}</span>
                                        @endif
                                        <!-- {{Form::date('date', \Carbon\Carbon::now())}} -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row" style="text-align: right; float:right">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Payment Method</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('method')) has-error @endif">
                                    @foreach($method as $key=>$val)
                                        
                                        <!-- {!! Form::radio('method_id', $v->id) !!} -->
                                        <input type="radio"  @if($income->method_id==$val->id) checked @endif name="method_id" value="{{$val->id}}" >
                                        <label for="nf-email">{{ $v->name_en }}</label>&nbsp;&nbsp;&nbsp;
                                        @endforeach
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Description</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('description')) has-error @endif">
                                    {!! Form::text('description', null, array('placeholder' => 'Please enter description','class' => 'form-control')) !!}
                                    @if($errors->first('description'))
                                            <span class="text-danger">{{$errors->first('description')}}</span>
                                    @endif
                                        <!-- {{Form::date('date', \Carbon\Carbon::now())}} -->
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-sm-12 col-lg-6">
                                
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Attachment file</label> </div>
                                    <div class="col-sm-12 @if($errors->has('choose')) has-error @endif">
                                    {!! Form::file('choose') !!}
                                    @if($errors->first('choose file'))
                                            <span class="text-danger">{{$errors->first('choose file')}}</span>
                                    @endif
                                    </div>
                                </div>
                            </div>
    
                        </div>
                       

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                    </div>

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
    <style>
        .right{
            float: right;
        }
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }

        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-tabs .nav-link{
            border: 0px solid #dadada;
            border-radius: 0px;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">

$('#data_3 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            format: "yyyy/mm/dd",
            showButtonPanel: true,
            gotoCurrent: true,
        });
        $('#data_3 .input-group.date').datepicker('setDate', new Date()); 
    </script>
@endsection


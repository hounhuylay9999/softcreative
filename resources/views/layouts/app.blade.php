<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $company->name_en }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{ asset('upload/'.$company->profile) }}">
    <link href="{{ asset('upload/'.$company->profile) }}" rel="apple-touch-icon">
    <!-- CoreUI CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" crossorigin="anonymous">
    <link href="{{asset('assets/css/hint.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/customize-style.css')}}" rel="stylesheet">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Nokora:400,700&amp;subset=khmer" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="c-app">
@include('layouts.sidebar')

<div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed">
        @include('layouts.header')
    </header>

    <div class="c-body">
        <main class="c-main">
            @yield('content')
        </main>
    </div>

    <footer class="c-footer">
       
        <div class="mfs-auto">Version 0.1 </div>
    </footer>
</div>
<style>
        body {
            font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif,'Nokora' !important;
        }
        .swal2-popup {
            font-size: 1.6rem !important;
        }
    </style>
<script src="{{ mix('js/app.js') }}" defer></script>
{{-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

<script>
    
//   //  $('li.c-sidebar-nav-dropdown').removeClass('c-sidebar-nav-item c-sidebar-nav-dropdown c-show');
//     $('.c-sidebar-nav li').click(function(e){
//         $(this).addClass('c-sidebar-nav-item c-sidebar-nav-dropdown c-show');
//     });
</script>
@yield('third_party_scripts')
@include('layouts.handle_js_css')

@include('layouts.message')
@stack('page_scripts')

@yield('scripts')

</body>
</html>

@php
        $url = url('/');
        $_jscss = '';

        $_lib = array(
            'select2'				=>
                '<link href="'.$url.'/assets/css/plugins/select2/select2.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/select2/select2.full.min.js"></script>',
            'highcharts'				=>
               '<link href="'.$url.'/assets/css/plugins/select2/select2.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts-more.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/solid-gauge.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/drilldown.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/exporting.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/export-data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/accessibility.js"></script>',
            'solid_gauge'				=>
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/highcharts-more.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/solid-gauge.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/drilldown.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/exporting.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/export-data.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/Highcharts-8.1.2/code/modules/accessibility.js"></script>',
            'highcharts_map'				=>
                '<script src="https://code.highcharts.com/maps/highmaps.js"></script>'.
                '<script src="'.$url.'/assets/js/kh_map.js"></script>',
            'chosen'				=>
                '<link href="'.$url.'/assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/chosen/chosen.jquery.js"></script>',
            'clockpicker'			=>
                '<link href="'.$url.'/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/clockpicker/clockpicker.js"></script>',
            'datepicker'			=>
                '<link href="'.$url.'/assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>',

            'dataTable'				=>
                '<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">'.
                '<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" defer></script>',

            'wizard'				=>
                '<link href="'.$url.'/assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/steps/jquery.steps.min.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/validate/jquery.validate.min.js"></script>',

            'c3'				=>
                '<link href="'.$url.'/assets/css/plugins/c3/c3.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/d3/d3.min.js"></script>'.
               '<script src="'.$url.'/assets/js/plugins/c3/c3.min.js"></script>',

            'moment'				=>
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>',

            'summernote'				=>
            '<link href="'.$url.'/assets/css/b-customize.css" rel="stylesheet">'.
                '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>'.
                '<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">'.
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>',

            'jasny-bootstrap'				=>
                '<link href="'.$url.'/assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>',
            'typeahead'				=>
                '<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>',
            'slick'				=>
                '<link href="'.$url.'/assets/css/plugins/slick/slick.css" rel="stylesheet">'.
                '<link href="'.$url.'/assets/css/plugins/slick/slick-theme.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/slick/slick.min.js"></script>',
            'fullcalendar'				=>
                '<link href="'.$url.'/assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">'.
                '<script src="'.$url.'/assets/js/plugins/fullcalendar/moment.min.js"></script>'.
                '<script src="'.$url.'/assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>',
            'publicjs'   =>
                '<script src="'.$url.'/assets/js/public.js"></script>',
        );


        $_libmap = array(
            //users
            'users.index'							    	=> $_lib['dataTable'],

            //roles
            'roles.index'							    	=> $_lib['dataTable'],

            //permission
            'permission.index'							    => $_lib['dataTable'],


            //project
            'project.index'                                       => $_lib['dataTable'],

            //Domain
            'domain.index'                                       => $_lib['dataTable'],

            //Hosting
            'hosting.index'                                       => $_lib['dataTable'],

            //Income Expense
            'income_expense.create'                               => $_lib['datepicker'],
            'income_expense.index'                                => $_lib['dataTable'],
            
            //report
            'report.index'                                       => $_lib['dataTable'].$_lib['datepicker'],

            //slide 
            'slide.create'                               => $_lib['datepicker'],
            'slide.index'                                => $_lib['dataTable'],

        );

        if( isset($_libmap[\Request::route()->getName()]) ){
            $_jscss				= $_libmap[\Request::route()->getName()];
        }
        echo $_jscss;
@endphp

<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
        data-class="c-sidebar-show">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>
<a class="c-header-brand d-lg-none c-header-brand-sm-up-center" href="#">
    <img src="{{ asset('upload/'.$company->profile) }}" width="118" alt="{{ $company->name_en }}">
</a>
<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
        data-class="c-sidebar-lg-show" responsive="true">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>
<ul class="c-header-nav mfs-auto">
</ul>
<ul class="c-header-nav">
    @can('notification-list')
    <li class="c-header-nav-item d-md-down-none mx-2">
        <a class="c-header-nav-link" href="#">
            <i class="c-icon mfe-2 cil-bell"></i>
            <span class="badge badge-pill badge-danger">0</span>
        </a>
    </li>
    @endcan
    <li class="c-header-nav-item dropdown">
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button"
           aria-haspopup="true" aria-expanded="false">
           <span class="m-r-sm text-muted welcome-message mr-2">

            @if(app()->getLocale() == 'en')
            <span>
                <img style=" width: 20px;  height: 20px; border-radius: 50%;" src="{{asset('assets/img/flat-english-01.png')}}">
            </span>
            @lang('message.english')
            @elseif(app()->getLocale() == 'kh')
                <span>
                    <img style=" width: 20px;  height: 20px; border-radius: 50%;"  src="{{asset('assets/img/flat-khmer-01.png')}}">
                </span>
                @lang('message.khmer')
            @endif 
            <b class="caret fas fa-angle-down"></b> </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2"><strong>@lang('message.language')</strong></div>
            <a class="dropdown-item" href="{{ url('locale/en') }}">
                <i class="c-icon mfe-2 cif-gb"></i>English
            </a>
            <a class="dropdown-item" href="{{ url('locale/kh') }}">
                <i class="c-icon mfe-2 cif-kh"></i>@lang('message.khmer')
            </a>
        </div>
    </li>
    <li class="c-header-nav-item dropdown">

        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button"
           aria-haspopup="true" aria-expanded="false">
           <span class="m-r-sm text-muted welcome-message mr-2">@lang('message.hi'),{{auth()->user()->name}}
            <b class="caret fas fa-angle-down"></b> </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2"><strong>@lang('message.account')</strong></div>
            <a class="dropdown-item" href="{{ route('profile.yourProfile') }}">
                <i class="c-icon mfe-2 cil-user"></i>@lang('message.profile')
            </a>
            <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="c-icon mfe-2 cil-account-logout"></i>@lang('message.logout')
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
</ul>
<div class="c-subheader justify-content-between px-3">
    @yield('breadcrumb')
</div>

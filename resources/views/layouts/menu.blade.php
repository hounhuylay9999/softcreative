
<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='home') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='home') c-active @endif" href="{{ route('home') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i>@lang('message.home')
    </a>
</li>





<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='project.index' || Route::currentRouteName()=='project.create' || Route::currentRouteName()=='project.edit') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='project.index' || Route::currentRouteName()=='project.create' || Route::currentRouteName()=='project.edit') c-active @endif" href="{{ route('project.index') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i>@lang('message.project')
    </a>
</li>

<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='slide.index' || Route::currentRouteName()=='slide.create' || Route::currentRouteName()=='slide.edit') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='slide.index' || Route::currentRouteName()=='slide.create' || Route::currentRouteName()=='slide.edit') c-active @endif" href="{{ route('slide.index') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i>@lang('message.slide')
    </a>
</li>


<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='domain.index' || Route::currentRouteName()=='domain.create' || Route::currentRouteName()=='domain.edit') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='domain.index' || Route::currentRouteName()=='domain.create' || Route::currentRouteName()=='domain.edit') c-active @endif" href="{{ route('domain.index') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i> @lang('message.domain_&_hosting')
    </a>
</li>

<li class="c-sidebar-nav-item @if(Route::currentRouteName()=='income_expense.index' || Route::currentRouteName()=='income_expense.create' || Route::currentRouteName()=='income_expense.edit') c-show @endif">
    <a class="c-sidebar-nav-link @if(Route::currentRouteName()=='income_expense.index' || Route::currentRouteName()=='income_expense.create' || Route::currentRouteName()=='income_expense.edit') c-active @endif" href="{{ route('income_expense.index') }}">
        <i class="c-sidebar-nav-icon cil-speedometer"></i> @lang('message.income_&_expense')
    </a>
</li>

<li class="c-sidebar-nav-item c-sidebar-nav-dropdown
@if(Route::currentRouteName()=='profile.companyProfile'
|| Route::currentRouteName()=='users.index' || Route::currentRouteName()=='users.create' || Route::currentRouteName()=='users.edit'
|| Route::currentRouteName()=='roles.index' || Route::currentRouteName()=='roles.create' || Route::currentRouteName()=='roles.edit'
|| Route::currentRouteName()=='permission.index') c-show @endif">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
        <i class="c-sidebar-nav-icon cil-cog"></i>@lang('message.setting')
    </a>
    <ul class="c-sidebar-nav-dropdown-items">

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='profile.companyProfile') c-active @endif" href="{{ route('profile.companyProfile') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.company_profile')</a></li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='users.index' || Route::currentRouteName()=='users.create' || Route::currentRouteName()=='users.edit') c-active @endif" href="{{ route('users.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_users')</a></li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='hosting.index' || Route::currentRouteName()=='hosting.create' || Route::currentRouteName()=='hosting.edit') c-active @endif" href="{{ route('hosting.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.hosting_where')</a></li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='report.index') c-active @endif" href="{{ route('report.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.report')</a></li>

        @can('role-list')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='roles.index' || Route::currentRouteName()=='roles.create' || Route::currentRouteName()=='roles.edit') c-active @endif" href="{{ route('roles.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_roles')</a></li>
        @endcan
        @can('role-list')
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link @if(Route::currentRouteName()=='permission.index') c-active @endif" href="{{ route('permission.index') }}"><span class="c-sidebar-nav-icon"></span> @lang('message.manage_permissions')</a></li>
        @endcan
    </ul>
</li>


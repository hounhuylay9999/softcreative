@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('project.index') }}">@lang('message.project')</a></li>
            <li class="breadcrumb-item active">@lang('message.modify')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('project.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::model($project, ['method' => 'PATCH','route' => ['project.update', $project->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.modify')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Project Name</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('project_name')) has-error @endif">
                                        {!! Form::text('project_name', null, array('placeholder' => 'Please enter project','class' => 'form-control')) !!}
                                        @if($errors->first('project_name'))
                                            <span class="text-danger">{{$errors->first('project_name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Project Price</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('price')) has-error @endif">
                                        {!! Form::text('price', null, ['placeholder' => 'Please enter price','class' => 'form-control']) !!}
                                        @if($errors->first('price'))
                                            <span class="text-danger">{{$errors->first('price')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Response By</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('response_by')) has-error @endif">
                                        <select name="response_by"  class="form-control select2" style="width: 100% !important;">
                                            <option value="" selected disable>__Please Select Response By__</option>

                                            @foreach($response as $k => $v)
                                                <option @if($project->response_by_id == $v->id) selected @endif  @if(old('response_by') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('response_by'))
                                            <span class="text-danger">{{$errors->first('response_by')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Team Develop</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('team_develop')) has-error @endif">
                                        <select name="team_develop[]"  class="form-control select2" style="width: 100% !important;" multiple>
                                            @foreach($response as $k => $v)
                                                <option @foreach($project_detail as $kk => $vv)  @if($vv->developer_id == $v->id) selected @endif   @endforeach value="{{ $v->id }}">{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('team_develop'))
                                            <span class="text-danger">{{$errors->first('team_develop')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.modify')</button>
                    </div>

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }

        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-tabs .nav-link{
            border: 0px solid #dadada;
            border-radius: 0px;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">


    </script>
@endsection

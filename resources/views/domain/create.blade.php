@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('domain.index') }}">@lang('message.domain')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('domain.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(array('route' => 'domain.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.domain_&_hosting')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Domain Name</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('domain_name')) has-error @endif">
                                        {!! Form::text('domain_name', null, array('placeholder' => 'Please enter domain','class' => 'form-control')) !!}
                                        @if($errors->first('domain_name'))
                                            <span class="text-danger">{{$errors->first('domain_name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Domain Price</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('domain_price')) has-error @endif">
                                        {!! Form::text('domain_price', null, ['placeholder' => 'Please enter domain_price','class' => 'form-control']) !!}
                                        @if($errors->first('domain_price'))
                                            <span class="text-danger">{{$errors->first('domain_price')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Hosting By</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('hosting_by')) has-error @endif">
                                        <select name="hosting_by"  class="form-control select2" style="width: 100% !important;">
                                            <option value="" selected disable>__Please Select Hosting By__</option>

                                            @foreach($response as $k => $v)
                                                <option @if(old('hosting_by') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('hosting_by'))
                                            <span class="text-danger">{{$errors->first('hosting_by')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Hosting Price</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('hosting_price')) has-error @endif">
                                        {!! Form::text('hosting_price', null, ['placeholder' => 'Please enter hosting_price','class' => 'form-control']) !!}
                                        @if($errors->first('hosting_price'))
                                            <span class="text-danger">{{$errors->first('hosting_price')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Hosting Where</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('hosting_where')) has-error @endif">
                                    <select name="hosting_where"  class="form-control" style="width: 100% !important;">
                                            <option selected disable>__Please Select Hosting Where__</option>
                                            @foreach($hosting as $k => $v)
                                                <option @if(old('hosting_where') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->hosting_name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('hosting_where'))
                                            <span class="text-danger">{{$errors->first('hosting_where')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Renew Date</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('renew')) has-error @endif">
                                        {!! Form::date('renew', null, array('placeholder' => 'Please enter date','class' => 'form-control')) !!}
                                        @if($errors->first('renew'))
                                            <span class="text-danger">{{$errors->first('renew')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Select Project</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('select_project')) has-error @endif">
                                        <select name="select_project"  class="form-control select2" style="width: 100% !important;">
                                            <option value="" selected disable>__Please Select Project__</option>

                                            @foreach($project as $k => $v)
                                                <option @if(old('select_project') == $v->id) selected @endif value="{{ $v->id }}">{{ $v->project_name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->first('select_project'))
                                            <span class="text-danger">{{$errors->first('select_project')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                    </div>

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }

        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-tabs .nav-link{
            border: 0px solid #dadada;
            border-radius: 0px;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">


    </script>
@endsection

@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('message.home')</a></li>
            <li class="breadcrumb-item"><a href="{{ route('hosting.index') }}">@lang('message.hosting_where')</a></li>
            <li class="breadcrumb-item active">@lang('message.add_new')</li>
        </ol>
        <div class="card-header-actions mt-1 float-right">
            <a class="btn btn-square btn-info addnew" href="{{ route('hosting.index') }}">@lang('message.back')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {!! Form::open(array('route' => 'hosting.store','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">@lang('message.hosting_where')</div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <div class="col-sm-12 control-label"><label for="nf-email">Hosting Name</label> <span class="text-danger">*</span></div>
                                    <div class="col-sm-12 @if($errors->has('hosting_name')) has-error @endif">
                                        {!! Form::text('hosting_name', null, array('placeholder' => 'Please enter hosting','class' => 'form-control')) !!}
                                        @if($errors->first('hosting_name'))
                                            <span class="text-danger">{{$errors->first('hosting_name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-md btn-primary btn-square" type="submit"> @lang('message.save')</button>
                    </div>

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
    <style>
        .select2-container .select2-selection--single{
            height: 34px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 34px !important;
        }

        .select2-container--default .select2-search--dropdown .select2-search__field{
            border: 1px solid #dadada !important;
            outline: blanchedalmond;
        }
        .note-editable{
            min-height: 200px !important;
        }
        .editable{
            max-width: 340px;
            object-fit:cover;
            border-radius: 5px;
        }
        .editable img{ max-width: 100%;}
        .btn-file{position: relative; margin-top: 10px}
        .btn-file>input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
            font-size: 23px;
            height: 100%;
            width: 100%;
            direction: ltr;
            cursor: pointer;
        }
        .img-circle{overflow: hidden;}
        img{max-width: 100%;}
        .nav-tabs .nav-link{
            border: 0px solid #dadada;
            border-radius: 0px;
        }
    </style>
@endsection


@section('scripts')
    <script type="text/javascript">


    </script>
@endsection

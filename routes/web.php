<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
      Session::put('locale', $locale);
      return redirect()->back();
});


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix'=>'admin','middleware' => ['auth']], function() {
    //Manage Users
    Route::GET('/users.html', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::GET('users/ajax/list', [App\Http\Controllers\UserController::class, 'getUserList'])->name('users.getUserList');
    Route::GET('/users/addnew.html', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
    Route::POST('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
    Route::DELETE('/users/delete/{id}.html', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');
    Route::GET('/users/modify/{id}.html', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::PATCH('/users/updated/{id}.html', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');

    //Manage Roles
    Route::GET('/role.html', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
    Route::GET('role/ajax/list', [App\Http\Controllers\RoleController::class, 'getRoleList'])->name('roles.getRoleList');
    Route::GET('/role/addnew.html', [App\Http\Controllers\RoleController::class, 'create'])->name('roles.create');
    Route::POST('/role/store', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
    Route::DELETE('/role/delete/{id}.html', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');
    Route::GET('/role/modify/{id}.html', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit');
    Route::PATCH('/role/updated/{id}.html', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');

    //Manage Profile
    Route::GET('your-profile.html', [App\Http\Controllers\ProfileController::class, 'yourProfile'])->name('profile.yourProfile');
    Route::POST('your-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'yourProfileUpdate'])->name('profile.yourProfileUpdate');
    Route::GET('company-profile.html', [App\Http\Controllers\ProfileController::class, 'companyProfile'])->name('profile.companyProfile');
    Route::POST('company-profile/modify/{id}.html', [App\Http\Controllers\ProfileController::class, 'companyProfileUpdate'])->name('profile.companyProfileUpdate');

    //Manage Permission
    Route::GET('/permission.html', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
    Route::GET('permission/ajax/list', [App\Http\Controllers\PermissionController::class, 'getPermissionList'])->name('permission.getPermissionList');




     //Project
     Route::GET('project.html', [App\Http\Controllers\ProjectController::class, 'index'])->name('project.index');
     Route::GET('project/ajax/list', [App\Http\Controllers\ProjectController::class, 'getProjectList'])->name('project.getProjectList');
     Route::GET('project/add-new.html', [App\Http\Controllers\ProjectController::class, 'create'])->name('project.create');
     Route::POST('project/store', [App\Http\Controllers\ProjectController::class, 'store'])->name('project.store');
     Route::DELETE('project/delete/{id}.html', [App\Http\Controllers\ProjectController::class, 'destroy'])->name('project.destroy');
     Route::GET('project/modify/{id}.html', [App\Http\Controllers\ProjectController::class, 'edit'])->name('project.edit');
     Route::PATCH('project/updated/{id}.html', [App\Http\Controllers\ProjectController::class, 'update'])->name('project.update');


      //domain
      Route::GET('domain.html', [App\Http\Controllers\DomainController::class, 'index'])->name('domain.index');
      Route::GET('domain/ajax/list', [App\Http\Controllers\DomainController::class, 'getDomainList'])->name('domain.getDomainList');
      Route::GET('domain/add-new.html', [App\Http\Controllers\DomainController::class, 'create'])->name('domain.create');
      Route::POST('domain/store', [App\Http\Controllers\DomainController::class, 'store'])->name('domain.store');
      Route::DELETE('domain/delete/{id}.html', [App\Http\Controllers\DomainController::class, 'destroy'])->name('domain.destroy');
      Route::GET('domain/modify/{id}.html', [App\Http\Controllers\DomainController::class, 'edit'])->name('domain.edit');
      Route::PATCH('domain/updated/{id}.html', [App\Http\Controllers\DomainController::class, 'update'])->name('domain.update');
 
      
      //hosting
      Route::GET('hosting.html', [App\Http\Controllers\HostingController::class, 'index'])->name('hosting.index');
      Route::GET('hosting/ajax/list', [App\Http\Controllers\HostingController::class, 'getHostingList'])->name('hosting.getHostingList');
      Route::GET('hosting/add-new.html', [App\Http\Controllers\HostingController::class, 'create'])->name('hosting.create');
      Route::POST('hosting/store', [App\Http\Controllers\HostingController::class, 'store'])->name('hosting.store');
      Route::DELETE('hosting/delete/{id}.html', [App\Http\Controllers\HostingController::class, 'destroy'])->name('hosting.destroy');
      Route::GET('hosting/modify/{id}.html', [App\Http\Controllers\HostingController::class, 'edit'])->name('hosting.edit');
      Route::PATCH('hosting/updated/{id}.html', [App\Http\Controllers\HostingController::class, 'update'])->name('hosting.update');
 
      //income_expense
      Route::GET('income_expense.html', [App\Http\Controllers\IncomeExpenseController::class, 'index'])->name('income_expense.index');
      Route::GET('income_expense/ajax/list', [App\Http\Controllers\IncomeExpenseController::class, 'getIncomeExpenseList'])->name('income_expense.getIncomeExpenseList');
      Route::GET('income_expense/add-new.html', [App\Http\Controllers\IncomeExpenseController::class, 'create'])->name('income_expense.create');
      Route::POST('income_expense/store', [App\Http\Controllers\IncomeExpenseController::class, 'store'])->name('income_expense.store');
      Route::DELETE('income_expense/delete/{id}.html', [App\Http\Controllers\IncomeExpenseController::class, 'destroy'])->name('income_expense.destroy');
      Route::GET('income_expense/modify/{id}.html', [App\Http\Controllers\IncomeExpenseController::class, 'edit'])->name('income_expense.edit');
      Route::PATCH('income_expense/updated/{id}.html', [App\Http\Controllers\IncomeExpenseController::class, 'update'])->name('income_expense.update');
 
      //report
      Route::GET('report.html', [App\Http\Controllers\ReportController::class, 'index'])->name('report.index');
      Route::GET('report/ajax/list', [App\Http\Controllers\ReportController::class, 'getReportList'])->name('report.getReportList');


      //Slide
     Route::GET('slide.html', [App\Http\Controllers\SlideController::class, 'index'])->name('slide.index');
     Route::GET('slide/ajax/list', [App\Http\Controllers\SlideController::class, 'getSlideList'])->name('slide.getSlideList');
     Route::GET('slide/add-new.html', [App\Http\Controllers\SlideController::class, 'create'])->name('slide.create');
     Route::POST('slide/store', [App\Http\Controllers\SlideController::class, 'store'])->name('slide.store');
     Route::DELETE('slide/delete/{id}.html', [App\Http\Controllers\SlideController::class, 'destroy'])->name('slide.destroy');
     Route::GET('slide/modify/{id}.html', [App\Http\Controllers\SlideController::class, 'edit'])->name('slide.edit');
     Route::PATCH('slide/updated/{id}.html', [App\Http\Controllers\SlideController::class, 'update'])->name('slide.update');
      
});


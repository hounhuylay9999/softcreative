<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income', function (Blueprint $table) {
            $table->id();
            $table->Integer('receipt')->nullable();
            $table->Integer('method_id')->nullable();
            $table->Integer('type_id')->nullable();
            $table->float('amount_usd',8,2)->nullable();
            $table->float('amount_khr',8,2)->nullable();
            $table->string('description')->nullable();
            $table->date('date')->nullable();
            $table->string('file_name')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->Integer('created_by')->nullable();
            $table->Integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income');
    }
}

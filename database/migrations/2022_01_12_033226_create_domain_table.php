<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain', function (Blueprint $table) {
            $table->id();
            $table->Integer('project_id')->nullable();
            $table->string('domain_name')->nullable();
            $table->float('domain_price',8,2)->nullable();
            $table->float('hosting_price',8,2)->nullable();
            $table->string('hosting_by')->nullable();
            $table->string('hosting_where')->nullable();
            $table->date('renew')->nullable();
            $table->tinyInteger('status')->default(1);

            $table->Integer('created_by')->nullable();
            $table->Integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_detail');
    }
}

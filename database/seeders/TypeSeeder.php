<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type')->insert([
            array(
             'name_en' => 'Income',
             'name_kh' => 'ចំណូល',
             'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Expense',
                'name_kh' => 'ចំណាយ',
                'created_at' => Carbon::now(),
            ),

         ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location')->insert([
            array(
             'name_en' => 'Banteay Meanchey',
             'name_kh' => 'បន្ទាយមានជ័យ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Battambang',
             'name_kh' => 'បាត់ដំបង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kampong Cham',
             'name_kh' => 'កំពង់ចាម',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kampong Chhnang',
             'name_kh' => 'កំពង់ឆ្នាំង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kampong Speu',
             'name_kh' => 'កំពង់ស្ពឺ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kampong Thom',
             'name_kh' => 'កំពង់ធំ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kampot',
             'name_kh' => 'កំពត',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kandal',
             'name_kh' => 'កណ្តាល',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Koh Kong',
             'name_kh' => 'កោះកុង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kratié',
             'name_kh' => 'ក្រចេះ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Mondulkiri',
             'name_kh' => 'មណ្ឌលគិរី',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Phnom Penh',
             'name_kh' => 'ភ្នំពេញ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Preah Vihear',
             'name_kh' => 'ព្រះវិហារ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Prey Veng',
             'name_kh' => 'ព្រៃវែង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Pursat',
             'name_kh' => 'ពោធិ៍សាត់',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Ratanak Kiri',
             'name_kh' => 'រតនគិរី',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Siem Reap',
             'name_kh' => 'សៀមរាប',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Preah Sihanouk',
             'name_kh' => 'ព្រះសីហនុ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Stung Treng',
             'name_kh' => 'ស្ទឹងត្រែង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Svay Rieng',
             'name_kh' => 'ស្វាយរៀង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Takéo',
             'name_kh' => 'តាកែវ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Oddar Meanchey',
             'name_kh' => 'ឧត្តរមានជ័យ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Kep',
             'name_kh' => 'កែប',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Pailin',
             'name_kh' => 'ប៉ៃលិន',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Tboung Khmum',
             'name_kh' => 'ត្បូងឃ្មុំ',
             'created_at' => Carbon::now(),
            )
        ]);
    }
}

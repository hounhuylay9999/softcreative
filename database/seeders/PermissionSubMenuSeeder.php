<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class PermissionSubMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_has_submenu')->insert([
            //users privilages
            array(
                'menu_permission_id'       => 1,
                'name'                     => 'Users',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 1,
                'name'                     => 'Roles',
                'created_at'               => Carbon::now(),
            ),
            //Order Manage
            array(
                'menu_permission_id'       => 2,
                'name'                     => 'Customer',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 2,
                'name'                     => 'Delivery',
                'created_at'               => Carbon::now(),
            ),
             //Payment Collection
             array(
                'menu_permission_id'       => 3,
                'name'                     => 'Payment',
                'created_at'               => Carbon::now(),
            ),
            //operation
            array(
                'menu_permission_id'       => 4,
                'name'                     => 'Employee',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 4,
                'name'                     => 'Payroll',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 4,
                'name'                     => 'Income/Expense',
                'created_at'               => Carbon::now(),
            ),
            //Report
            array(
                'menu_permission_id'       => 5,
                'name'                     => 'Report',
                'created_at'               => Carbon::now(),
            ),
            //Setting
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Company Profile',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Manage Zone Types',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Manage Comission',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Manage Zone',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Manage Department',
                'created_at'               => Carbon::now(),
            ),
            array(
                'menu_permission_id'       => 6,
                'name'                     => 'Manage Position',
                'created_at'               => Carbon::now(),
            ),
            //Menu
            array(
                'menu_permission_id'       => 7,
                'name'                     => 'Menu List',
                'created_at'               => Carbon::now(),
            ),
         ]);
    }
}
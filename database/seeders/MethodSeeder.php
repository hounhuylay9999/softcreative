<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class MethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('method')->insert([
            array(
             'name_en' => 'Bank',
             'name_kh' => 'ធនាគារ',
             'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Cash',
                'name_kh' => 'សាច់ប្រាក់',
                'created_at' => Carbon::now(),
            ),

         ]);
    }
}

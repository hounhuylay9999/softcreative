<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            array(
             'name_en' => 'Design',
             'name_kh' => 'ឌីហ្សាញ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Properties',
             'name_kh' => 'អចលនទ្រព្យ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Development',
             'name_kh' => 'ការអភិវឌ្ឍន៍',
             'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Villa lux home',
                'name_kh' => 'ផ្ទះវីឡាប្រណីត',
                'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Project Opening',
                'name_kh' => 'ពិធីបើកកម្រោង',
                'created_at' => Carbon::now(),
               )
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gender')->insert([
           array(
            'name_en' => 'Male',
            'name_kh' => 'ប្រុស',
            'created_at' => Carbon::now(),
           ),
           array(
            'name_en' => 'Female',
            'name_kh' => 'ស្រី',
            'created_at' => Carbon::now(),
           )
        ]);
    }
}

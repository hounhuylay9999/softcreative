<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GenderSeeder::class,
            UserSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            PermissionMenuSeeder::class,
            PermissionSubMenuSeeder::class,
            ModelHasRoleSeeder::class,
            CompanySeeder::class,
            LocationSeeder::class,
            TypeSeeder::class,
            MethodSeeder::class,


        ]);
    }
}

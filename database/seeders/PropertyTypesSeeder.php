<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class PropertyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('property_types')->insert([
            array(
             'name_en' => 'Apartment Building',
             'name_kh' => 'អាគារផ្ទះល្វែង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Apartment',
             'name_kh' => 'ផ្ទះល្វែង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Office',
             'name_kh' => 'ការិយាល័យ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Shop',
             'name_kh' => 'ហាង',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Villa',
             'name_kh' => 'វីឡា',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Condos',
             'name_kh' => 'ខុនដូ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Houses',
             'name_kh' => 'ផ្ទះ',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Land',
             'name_kh' => 'ដី',
             'created_at' => Carbon::now(),
            ),

        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class GeneralAmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_amenities')->insert([
            array(
             'name_en' => 'Garden',
             'name_kh' => 'សួនច្បា',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => '24hr Security',
             'name_kh' => 'សន្តិសុខ 24 ម៉ោង',
             'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Swimming Pool',
            'name_kh' => 'អាងហែលទឹក',
            'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Pet friendly',
            'name_kh' => 'មានសត្វចិញ្ចឹម',
            'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Tennis Court',
            'name_kh' => 'ទីលាន​វាយ​តេន​នី​ស',
            'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Balcony sea view',
            'name_kh' => 'Balcony sea view',
            'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Close to local Schools',
            'name_kh' => 'ជិតសាលារៀន',
            'created_at' => Carbon::now(),
            ),
            array(
            'name_en' => 'Access controlled',
            'name_kh' => 'គ្រប់គ្រងការចូលប្រើប្រាស់',
            'created_at' => Carbon::now(),
            ),
        ]);
    }
}

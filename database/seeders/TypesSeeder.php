<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            array(
             'name_en' => 'For Sale',
             'name_kh' => 'សម្រាប់​លក់',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'For Rent',
             'name_kh' => 'សម្រាប់​ជួល',
             'created_at' => Carbon::now(),
            )
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            array(
                'name'       => 'Admin',
                'email'      => 'admin@admin.com',
                'entry_by'   => 1,
                'password'   => Hash::make(12345678),
                'gender'     => 1,
                'profile'    => 'delivery.png',
                'created_at' => Carbon::now(),
            ),
         ]);
    }
}

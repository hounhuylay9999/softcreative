<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class GalleryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gallery_type')->insert([
            array(
             'name_en' => 'Living Room',
             'name_kh' => 'បន្ទប់ទទួលភ្ញៀវ',
             'slug' => 'living',
             'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Driveway',
                'name_kh' => 'ផ្លូវថ្នល់',
                'slug' => 'driveway',
                'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Bedroom',
                'name_kh' => 'បន្ទប់គេង',
                'slug' => 'bedroom',
                'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Garage',
                'name_kh' => 'យានដ្ឋាន',
                'slug' => 'garage',
                'created_at' => Carbon::now(),
            ),
            array(
                'name_en' => 'Kitchen',
                'name_kh' => 'ផ្ទះបាយ',
                'slug' => 'kitchen',
                'created_at' => Carbon::now(),
               )

         ]);
    }
}

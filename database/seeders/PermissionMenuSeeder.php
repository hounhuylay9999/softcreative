<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class PermissionMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_menu')->insert([
            array(
                'name'       => 'Users Privillage',
                'created_at' => Carbon::now(),
            ),
            array(
                'name'       => 'Manage Order',
                'created_at' => Carbon::now(),
               ),
            array(
                'name'       => 'Payment Collection',
                'created_at' => Carbon::now(),
            ),
            array(
                'name'       => 'Operation',
                'created_at' => Carbon::now(),
            ),
            array(
                'name'       => 'Report',
                'created_at' => Carbon::now(),
            ),
            array(
                'name'       => 'Setting',
                'created_at' => Carbon::now(),
            ),
            array(
                'name'       => 'Menu',
                'created_at' => Carbon::now(),
            ),
         ]);
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            array(
             'name_en' => 'Available',
             'name_kh' => 'មាន',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Urgent',
             'name_kh' => 'បន្ទាន់',
             'created_at' => Carbon::now(),
            ),
            array(
             'name_en' => 'Sold Out',
             'name_kh' => 'លក់​ហើយ',
             'created_at' => Carbon::now(),
            )
        ]);
    }
}

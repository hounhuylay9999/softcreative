-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.31 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table admin_real_estate.company_profile
DROP TABLE IF EXISTS `company_profile`;
CREATE TABLE IF NOT EXISTS `company_profile` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `profile` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_object` json DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.company_profile: 1 rows
/*!40000 ALTER TABLE `company_profile` DISABLE KEYS */;
INSERT INTO `company_profile` (`id`, `profile`, `name_en`, `name_kh`, `email`, `company_object`, `address`, `phone`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'delivery.png', 'Core Back End', 'Core Back End', 'admin@admin.com', '{"CompanyCode": "100"}', '#80A, st 25 Borey Chomka Dong New World, Phnom Penh, Cambodia.', '093322910', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `company_profile` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.failed_jobs: 0 rows
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.gender
DROP TABLE IF EXISTS `gender`;
CREATE TABLE IF NOT EXISTS `gender` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.gender: 2 rows
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` (`id`, `name_en`, `name_kh`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Male', 'ប្រុស', 1, '2021-12-28 16:17:22', NULL),
	(2, 'Female', 'ស្រី', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.general
DROP TABLE IF EXISTS `general`;
CREATE TABLE IF NOT EXISTS `general` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `header_logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_phone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_fb_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `header_yt_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_head` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_head_desc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_branch` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_branch_desc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_call` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_call_1` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_call_2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_email_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_web_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_copyright` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_cover` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_cover` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_cover` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `news_cover` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `lang` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.general: 0 rows
/*!40000 ALTER TABLE `general` DISABLE KEYS */;
/*!40000 ALTER TABLE `general` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.herobanner
DROP TABLE IF EXISTS `herobanner`;
CREATE TABLE IF NOT EXISTS `herobanner` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_title_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `title_kh` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_title_kh` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc_kh` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `banner` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.herobanner: 0 rows
/*!40000 ALTER TABLE `herobanner` DISABLE KEYS */;
/*!40000 ALTER TABLE `herobanner` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.migrations: 10 rows
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2021_06_16_150951_create_permission_tables', 1),
	(5, '2021_06_17_050822_create_gender_table', 1),
	(6, '2021_06_17_072304_create_permission_menu_table', 1),
	(7, '2021_06_17_072339_create_permission_has_submenu_table', 1),
	(8, '2021_06_17_140149_create_company_profile_table', 1),
	(9, '2021_08_22_033208_create_general_table', 1),
	(10, '2021_09_17_032247_create_herobanner_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.model_has_permissions
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.model_has_permissions: 0 rows
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.model_has_roles
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.model_has_roles: 1 rows
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Models\\User', 1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.password_resets: 0 rows
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_group_id` tinyint(4) DEFAULT NULL,
  `menu_sub_group_id` tinyint(4) DEFAULT NULL,
  `name_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name_kh` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guard_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_en_unique` (`name_en`),
  UNIQUE KEY `permissions_name_kh_unique` (`name_kh`),
  UNIQUE KEY `permissions_name_slug_unique` (`name`,`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.permissions: 70 rows
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `menu_group_id`, `menu_sub_group_id`, `name_en`, `name_kh`, `slug`, `name`, `guard_name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'view users', 'បង្ហាញអ្នកប្រើប្រាស់ទាំងអស់', 'user-list', 'user-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(2, 1, 1, 'add new users', 'បង្កើតអ្នកប្រើប្រាស់ថ្មី', 'user-create', 'user-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(3, 1, 1, 'modify users', 'កែប្រែអ្នកប្រើប្រាស់ថ្មី', 'user-edit', 'user-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(4, 1, 1, 'delete users', 'លុបអ្នកប្រើប្រាស់', 'user-delete', 'user-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(5, 1, 2, 'view roles', 'view roles', 'role-list', 'role-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(6, 1, 2, 'add new roles', 'add new roles', 'role-create', 'role-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(7, 1, 2, 'modify roles', 'modify roles', 'role-edit', 'role-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(8, 1, 2, 'delete roles', 'delete roles', 'role-delete', 'role-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(9, 6, 10, 'view company profile', 'view company profile', 'company-list', 'company-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(10, 6, 10, 'modify company profile', 'modify company profile', 'company-edit', 'company-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(11, 2, 3, 'View Customer', 'View Customer', 'customer-list', 'customer-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(12, 2, 3, 'Add New Customer', 'Add New Customer', 'customer-create', 'customer-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(13, 2, 3, 'Modify Customer', 'Modify Customer', 'customer-edit', 'customer-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(14, 2, 3, 'Delete Customer', 'Delete Customer', 'customer-delete', 'customer-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(15, 2, 4, 'View Delivery', 'View Delivery', 'delivery-list', 'delivery-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(16, 2, 4, 'Add New Delivery', 'Add New Delivery', 'delivery-create', 'delivery-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(17, 2, 4, 'Modify Delivery', 'Modify Delivery', 'delivery-edit', 'delivery-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(18, 2, 4, 'Delete Delivery', 'Delete Delivery', 'delivery-delete', 'delivery-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(19, 2, 4, 'Delivery Arrange', 'Delivery Arrange', 'delivery-arrange', 'delivery-arrange', 'web', 1, '2021-12-28 16:17:22', NULL),
	(20, 2, 4, 'Delivery Print', 'Delivery Print', 'delivery-print', 'delivery-print', 'web', 1, '2021-12-28 16:17:22', NULL),
	(21, 3, 5, 'Collect Payment with Staff', 'Collect Payment with Staff', 'collect-payment-staff', 'collect-payment-staff', 'web', 1, '2021-12-28 16:17:22', NULL),
	(22, 3, 5, 'Collect Payment Shop', 'Collect Payment Shop', 'collect-payment-shop', 'collect-payment-shop', 'web', 1, '2021-12-28 16:17:22', NULL),
	(23, 3, 5, 'Confirm Payment with Staff', 'Confirm Payment with Staff', 'confirm-payment-staff', 'confirm-payment-staff', 'web', 1, '2021-12-28 16:17:22', NULL),
	(24, 3, 5, 'Confirm Payment with Shop', 'Confirm Payment with Shop', 'confirm-payment-shop', 'confirm-payment-shop', 'web', 1, '2021-12-28 16:17:22', NULL),
	(25, 4, 6, 'View Employee', 'View Employee', 'employee-list', 'employee-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(26, 4, 6, 'Add New Employee', 'Add New Employee', 'employee-create', 'employee-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(27, 4, 6, 'Modify Employee', 'Modify Employee', 'employee-edit', 'employee-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(28, 4, 6, 'Delete Employee', 'Delete Employee', 'employee-delete', 'employee-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(29, 4, 6, 'Add Contract', 'Add Contract', 'add-contract', 'add-contract', 'web', 1, '2021-12-28 16:17:22', NULL),
	(30, 4, 6, 'Modify Contract', 'Modify Contract', 'modify-contract', 'modify-contract', 'web', 1, '2021-12-28 16:17:22', NULL),
	(31, 4, 7, 'View Payroll', 'View Payroll', 'payroll-list', 'payroll-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(32, 4, 7, 'Generated Payroll', 'Generated Payroll', 'payroll-create', 'payroll-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(33, 4, 7, 'Submit Payroll', 'Submit Payroll', 'payroll-submit', 'payroll-submit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(34, 4, 7, 'Print Payroll', 'Print Payroll', 'payroll-print', 'payroll-print', 'web', 1, '2021-12-28 16:17:22', NULL),
	(35, 4, 8, 'View Income & Expense Transaction', 'View Income & Expense Transaction', 'income-expense-list', 'income-expense-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(36, 4, 8, 'Delete Income & Expense Transaction', 'Delete Income & Expense Transaction', 'income-expense-delete', 'income-expense-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(37, 4, 8, 'Add New Income & Expense Transaction', 'Add New Income & Expense Transaction', 'income-expense-create', 'income-expense-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(38, 5, 9, 'Income Report', 'Income Report', 'income-report-list', 'income-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(39, 5, 9, 'Expense Report', 'Expense Report', 'expense-report-list', 'expense-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(40, 5, 9, 'Shop Report', 'Shop Report', 'shop-report-list', 'shop-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(41, 5, 9, 'Delivery Report', 'Delivery Report', 'delivery-report-list', 'delivery-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(42, 5, 9, 'Export Report', 'Export Report', 'export-report-list', 'export-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(43, 5, 9, 'Import Report', 'Import Report', 'import-report-list', 'import-report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(44, 6, 11, 'view zone types', 'view zone types', 'zone-type-list', 'zone-type-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(45, 6, 11, 'add new zone types', 'add new zone types', 'zone-type-create', 'zone-type-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(46, 6, 11, 'modify zone types', 'modify zone types', 'zone-type-edit', 'zone-type-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(47, 6, 11, 'delete zone types', 'delete zone types', 'zone-type-delete', 'zone-type-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(48, 6, 12, 'view Commission', 'view Commission', 'commission-list', 'commission-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(49, 6, 12, 'add new Commission', 'add new Commission', 'commission-create', 'commission-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(50, 6, 12, 'modify Commission', 'modify Commission', 'commission-edit', 'commission-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(51, 6, 12, 'delete Commission', 'delete Commission', 'commission-delete', 'commission-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(52, 6, 13, 'view zone', 'view zone', 'zone-list', 'zone-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(53, 6, 13, 'add new zone', 'add new zone', 'zone-create', 'zone-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(54, 6, 13, 'modify zone', 'modify zone', 'zone-edit', 'zone-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(55, 6, 13, 'delete zone', 'delete zone', 'zone-delete', 'zone-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(56, 6, 14, 'view department', 'view department', 'department-list', 'department-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(57, 6, 14, 'add new department', 'add new department', 'department-create', 'department-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(58, 6, 14, 'modify department', 'modify department', 'department-edit', 'department-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(59, 6, 14, 'delete department', 'delete department', 'department-delete', 'department-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(60, 6, 15, 'view position', 'view position', 'position-list', 'position-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(61, 6, 15, 'add new position', 'add new position', 'position-create', 'position-create', 'web', 1, '2021-12-28 16:17:22', NULL),
	(62, 6, 15, 'modify position', 'modify position', 'position-edit', 'position-edit', 'web', 1, '2021-12-28 16:17:22', NULL),
	(63, 6, 15, 'delete position', 'delete position', 'position-delete', 'position-delete', 'web', 1, '2021-12-28 16:17:22', NULL),
	(64, 7, 16, 'Manage Order', 'Manage Order', 'manage-order-list', 'manage-order-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(65, 7, 16, 'Payment Collection', 'Payment Collection', 'payment-collection-list', 'payment-collection-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(66, 7, 16, 'Operation', 'Operation', 'operation-list', 'operation-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(67, 7, 16, 'HR Module', 'HR Module', 'hr-module-list', 'hr-module-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(68, 7, 16, 'Report', 'Report', 'report-list', 'report-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(69, 7, 16, 'Setting', 'Setting', 'setting-list', 'setting-list', 'web', 1, '2021-12-28 16:17:22', NULL),
	(70, 7, 16, 'Notification', 'Notification', 'notification-list', 'notification-list', 'web', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.permission_has_submenu
DROP TABLE IF EXISTS `permission_has_submenu`;
CREATE TABLE IF NOT EXISTS `permission_has_submenu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_permission_id` tinyint(4) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.permission_has_submenu: 16 rows
/*!40000 ALTER TABLE `permission_has_submenu` DISABLE KEYS */;
INSERT INTO `permission_has_submenu` (`id`, `menu_permission_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Users', 1, '2021-12-28 16:17:22', NULL),
	(2, 1, 'Roles', 1, '2021-12-28 16:17:22', NULL),
	(3, 2, 'Customer', 1, '2021-12-28 16:17:22', NULL),
	(4, 2, 'Delivery', 1, '2021-12-28 16:17:22', NULL),
	(5, 3, 'Payment', 1, '2021-12-28 16:17:22', NULL),
	(6, 4, 'Employee', 1, '2021-12-28 16:17:22', NULL),
	(7, 4, 'Payroll', 1, '2021-12-28 16:17:22', NULL),
	(8, 4, 'Income/Expense', 1, '2021-12-28 16:17:22', NULL),
	(9, 5, 'Report', 1, '2021-12-28 16:17:22', NULL),
	(10, 6, 'Company Profile', 1, '2021-12-28 16:17:22', NULL),
	(11, 6, 'Manage Zone Types', 1, '2021-12-28 16:17:22', NULL),
	(12, 6, 'Manage Comission', 1, '2021-12-28 16:17:22', NULL),
	(13, 6, 'Manage Zone', 1, '2021-12-28 16:17:22', NULL),
	(14, 6, 'Manage Department', 1, '2021-12-28 16:17:22', NULL),
	(15, 6, 'Manage Position', 1, '2021-12-28 16:17:22', NULL),
	(16, 7, 'Menu List', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `permission_has_submenu` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.permission_menu
DROP TABLE IF EXISTS `permission_menu`;
CREATE TABLE IF NOT EXISTS `permission_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.permission_menu: 7 rows
/*!40000 ALTER TABLE `permission_menu` DISABLE KEYS */;
INSERT INTO `permission_menu` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Users Privillage', 1, '2021-12-28 16:17:22', NULL),
	(2, 'Manage Order', 1, '2021-12-28 16:17:22', NULL),
	(3, 'Payment Collection', 1, '2021-12-28 16:17:22', NULL),
	(4, 'Operation', 1, '2021-12-28 16:17:22', NULL),
	(5, 'Report', 1, '2021-12-28 16:17:22', NULL),
	(6, 'Setting', 1, '2021-12-28 16:17:22', NULL),
	(7, 'Menu', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `permission_menu` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.roles: 1 rows
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `slug`, `guard_name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'administrator', 'administrator', 'web', 1, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.role_has_permissions
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.role_has_permissions: 0 rows
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table admin_real_estate.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `entry_by` tinyint(4) DEFAULT NULL,
  `update_by` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table admin_real_estate.users: 1 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `profile`, `gender`, `email_verified_at`, `password`, `status`, `entry_by`, `update_by`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@admin.com', 'delivery.png', 1, NULL, '$2y$10$tJAsKl0.d.QkDNJmWZ7DbeDhPvWqWein1bPVbrv9S27Wj4bdny4eC', 1, 1, NULL, NULL, '2021-12-28 16:17:22', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

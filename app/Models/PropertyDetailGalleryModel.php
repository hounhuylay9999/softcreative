<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyDetailGalleryModel extends Model
{
    use HasFactory;
    protected $table ="property_detail_gallery";
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogoClientModel extends Model
{
    use HasFactory;
    protected $table = "logo_client";
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainModel extends Model
{
    use HasFactory;
    protected $table ="domain";

    public function get_response(){
        return $this->hasOne(User::class,'id','hosting_by');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionSubMenuModel extends Model
{
    use HasFactory;
    protected $table = 'permission_has_submenu';
    protected $primaryKey = 'id';
    protected $fillable = ['name','status','menu_permission_id'];
}

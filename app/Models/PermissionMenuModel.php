<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionMenuModel extends Model
{
    use HasFactory;
    protected $table = 'permission_menu';
    protected $primaryKey = 'id';
    protected $fillable = ['name','status'];

    public function getPermissionByGroup(){
        return $this->hasMany(PermissionModel::class,'menu_group_id','id');
    }


}

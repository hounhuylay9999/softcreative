<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;
class GeneralModel extends Model
{
    use HasFactory;
    protected $table = 'general';
    public static function head_office(){
        $lang_code = App::getLocale();
        $head_office = GeneralModel::where('lang',$lang_code)->first();

        return $head_office;

    }
    public static function phbs_branch(){
        $lang_code = App::getLocale();
        $phbs_branch = GeneralModel::where('lang',$lang_code)->first();

        return $phbs_branch;
    }
    public static function call_us(){
        $lang_code = App::getLocale();
        $call_us = GeneralModel::where('lang',$lang_code)->first();

        return $call_us;
    }
    public static function email_us(){
        $lang_code = App::getLocale();
        $email_us = GeneralModel::where('lang',$lang_code)->first();

        return $email_us;
    }
}
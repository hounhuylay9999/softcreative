<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectModel extends Model
{
    use HasFactory;
    protected $table ="project";

    public function get_response(){
        return $this->hasOne(User::class,'id','response_by_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionModel extends Model
{
    use HasFactory;
    protected $table="permissions";
    protected $primaryKey='id';
    protected $fillable=['menu_group_id','menu_sub_group_id','name','status'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncomeExpenseModel extends Model
{
    use HasFactory;
    protected $table ="income";
    public function get_response(){
        return $this->hasOne(User::class,'id','created_by');
    }

    public function get_method(){
        return $this->hasOne(MethodModel::class,'id','method_id')->select(['id','name_en']);
    }

    public function get_type(){
        return $this->hasOne(TypeModel::class,'id','type_id')->select(['id','name_en']);
    }
  
}

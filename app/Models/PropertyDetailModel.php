<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyDetailModel extends Model
{
    use HasFactory;
    protected $table = "property_detail";
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\CompanyModel;
use Auth;
use App\Models\GeneralModel;
use App\Models\PropertyTypesModel;
use App\Models\GalleryModel;
use App;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer(['layouts.app'], function($view)
        {
            $company = CompanyModel::where('status',1)->first();
            $view->with(compact('company'));
        });

        view()->composer(['front-end.layouts.app'], function($view)
        {
            $cate = PropertyTypesModel::leftJoin('property_detail','property_detail.property_types_id','property_types.id')
            ->leftJoin('property','property.id','property_detail.property_id')
            ->select([
                'property_types.name_'.App::getLocale().' as name',
                DB::raw('count(property.id) as total'),
                'property_types.id'
                ])
            ->where('property_types.status',1)
            //->groupBy('property.id')
            ->groupBy('property_types.id')->orderBy('total','DESC');
            //$category = $cate->get();
            $cat = $cate->limit(5)->get();
           // dd($cat);
            $general = GeneralModel::select([
                'title','phone','email','slogan','header_logo','favicon',
                'description_'.App::getLocale().' as description',
                'meta_keyword_'.App::getLocale().' as meta_keyword',
                'meta_description_'.App::getLocale().' as meta_description',
                'facebook_url',
                'youtube_url',
                'twitter_url',
                'google_url',
                'telegram_url',
                'linkedin_url',
                'address_'.App::getLocale().' as address',
                'phone','email','fax'

            ])->where('status',1)->first();

            $gallery_footer       = GalleryModel::Join('gallery_detail','gallery_detail.gallery_id','gallery.id')
            ->join('gallery_type','gallery_type.id','gallery_detail.gallery_type_id')
            ->select([
                'gallery.id',
                'gallery.title_'.App::getLocale().' as title',
                'gallery.description_'.App::getLocale().' as description',
                'gallery.thumbnail',
                'gallery_detail.gallery_type_id',
                'gallery_type.slug',
                DB::raw('group_concat(gallery_type.slug) as slug_name'),
                DB::raw('group_concat(gallery_type.name_'.App::getLocale().') as name')
            ])->where('gallery.status',1)->groupBy('gallery.id')->limit(6)->get();

            $view->with(compact('general','cat','gallery_footer'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(250);
    }
}

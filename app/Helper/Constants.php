<?php
namespace App\Helper;

class Constants
{
  
  public static $FLAGS = [
        'REJECT'    => -2,
        'INACTIVE'  => -1,
        'PENDING'   => 0,
        'ACTIVE'    => 1,
        'REVIEW'    => 2,
        'APPROVED'  => 3,
        'REQUEST'   => 4,
    ];

}
<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GalleryModel;
use App\Models\GalleryTypesModel;
use App;
use DB;
class GalleryPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $gallery       = GalleryModel::Join('gallery_detail','gallery_detail.gallery_id','gallery.id')
        ->join('gallery_type','gallery_type.id','gallery_detail.gallery_type_id')
        ->select([
            'gallery.id',
            'gallery.title_'.App::getLocale().' as title',
            'gallery.description_'.App::getLocale().' as description',
            'gallery.thumbnail',
            'gallery_detail.gallery_type_id',
            'gallery_type.slug',
            DB::raw('group_concat(gallery_type.slug) as slug_name'),
            DB::raw('group_concat(gallery_type.name_'.App::getLocale().') as name')
        ])->where('gallery.status',1)->groupBy('gallery.id')->limit(30)->get();
       // dd($gallery);
        $gallery_types       = GalleryTypesModel::select([
            'name_'.App::getLocale().' as name',
            'slug',
        ])->where('status',1)->get();

        return view('front-end.page.gallery',compact('gallery','gallery_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

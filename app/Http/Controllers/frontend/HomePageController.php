<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HerobannerModel;
use App;
use DB;
use App\Models\StatusModel;
use App\Models\TypesModel;
use App\Models\LocationModel;
use App\Models\PropertyTypesModel;
use App\Models\PropertyHightlightModel;
use App\Models\PropertyModel;
use App\Models\NewsModel;
use App\Models\User;
use App\Models\LogoClientModel;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function HomePage()
    {
        $herobanner     = HerobannerModel::select([
            'banner',
            'title_'.App::getLocale().' as title',
            'short_title_'.App::getLocale().' as short_title',
            'short_description_'.App::getLocale().' as short_description',
        ])->where('status',1)->orderBy('id','desc')->get();

        $location = LocationModel::where('status',1)->get();
        $types    = TypesModel::where('status',1)->get();
        $status   = StatusModel::where('status',1)->get();
        $property_types = PropertyTypesModel::where('status',1)->get();


        $property       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.sale_price',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'property.types_id',
            'property.status_id',
            'status.name_'.App::getLocale().' as status_name',
            'users.name as user',
            'property.created_at'
        ])->where('property.status',1)->whereIn('status_id',[1,2])->where('types_id',1)->orderBy('property.id','DESC')->limit(6)->get();



        $property_listing       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.sale_price',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'property.types_id',
            'property.status_id',
            'status.name_'.App::getLocale().' as status_name',
            'users.name as user',
            'property.created_at'
        ])->where('property.status',1)->inRandomOrder()->limit(12)->get();;


        $news       = NewsModel::Join('users','users.id','news.created_by')
        ->select([
            'news.id',
            'news.title_'.App::getLocale().' as title',
            'news.thumbnail',
            'news.description_'.App::getLocale().' as description',
            'users.name as user',
            'users.position',
            'users.profile',
            'users.description as user_description',
            'news.created_at'
        ])->where('news.status',1)->limit(3)->get();


        $agency = User::where('status',1)->limit(4)->get();
        $logo = LogoClientModel::where('status',1)->get();
        $prop =  PropertyHightlightModel::select([
            'description_'.App::getLocale().' as description',
            'title_quiet_'.App::getLocale().' as title_quiet',
            'description_quiet_'.App::getLocale().' as description_quiet',
            'title_great_'.App::getLocale().' as title_great',
            'description_great_'.App::getLocale().' as description_great',
            'title_view_'.App::getLocale().' as title_view',
            'description_view_'.App::getLocale().' as description_view',
            'title_large_'.App::getLocale().' as title_large',
            'description_large_'.App::getLocale().' as description_large',
        ])->where('status',1)->first();


        //feature_property
        $feature_property_h       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'users.name as user'
        ])->where('property.status',1)
        ->where('property.feature_property','yes')
        ->orderBy('property.id','DESC')->limit(4)->get();

        return view('front-end.page.home',compact('feature_property_h','prop','herobanner','location','types','status','property_types','property','property_listing','news','agency','logo'));
    }


}

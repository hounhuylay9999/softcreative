<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PropertyModel;
use App\Models\PropertyDetailModel;
use App\Models\PropertyTypesModel;
use App\Models\PropertyAmenitiesDetailModel;
use App\Models\GeneralAmenitiesModel;
use App\Models\NewsModel;
use App\Models\PropertyDetailGalleryModel;
use App;
use DB;
class PropertyPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $property       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.sale_price',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'status.name_'.App::getLocale().' as status_name',
            'property.created_at',
            'users.name',
            'users.profile',
            'users.phone_number',
            'users.fax',
            'users.email',
            'users.description',
            'property.google_maps'
        ])->where('property.status',1)->orderBy('property.id','DESC')->first();

        $gallery_detail = PropertyDetailGalleryModel::where('property_id',$property->id)->get();

        $property_type     = PropertyDetailModel::join('property_types','property_types.id','property_detail.property_types_id')->select(['property_types.name_'.App::getLocale().' as name'])->where('property_id',$property->id)->get();
        $general_amenities = PropertyAmenitiesDetailModel::join('general_amenities','general_amenities.id','property_amenities_detail.amenities_id')->select(['general_amenities.name_'.App::getLocale().' as name'])->where('property_id',$property->id)->get();

        //lated
        $latest_property       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.sale_price',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'status.name_'.App::getLocale().' as status_name',
            'property.created_at',
            'users.name as user'
        ])->where('property.status',1)->whereNotIn('property.id',[$property->id])
        ->orderBy('property.id','DESC')->limit(4)->get();

         //feature_property
         $feature_property       = PropertyModel::Join('location','location.id','property.location_id')
         ->Join('types','types.id','property.types_id')
         ->Join('status','status.id','property.status_id')
         ->Join('users','users.id','property.created_by')
         ->select([
             'property.id',
             'property.title_'.App::getLocale().' as title',
             'property.sale_price','property.thumbnail',
             'property.description_'.App::getLocale().' as description',
             'location.name_'.App::getLocale().' as location',
             'property.land_size_'.App::getLocale().' as land_size',
             'property.building_size_'.App::getLocale().' as building_size',
             'property.parking_'.App::getLocale().' as parking',
             'property.bedroom_'.App::getLocale().' as bedroom',
             'property.bathroom_'.App::getLocale().' as bathroom',
             'property.living_room_'.App::getLocale().' as living_room',
             'property.legal_document_'.App::getLocale().' as legal_document',
             'property.property_owner_'.App::getLocale().' as property_owner',
             'types.name_'.App::getLocale().' as types_name',
             'status.name_'.App::getLocale().' as status_name',
             'property.created_at',
             'users.name as user'
         ])->where('property.status',1)
         ->where('property.feature_property','yes')
         ->orderBy('property.id','DESC')->limit(3)->get();

         $news_latest       = NewsModel::Join('users','users.id','news.created_by')
         ->select([
             'news.id',
             'news.title_'.App::getLocale().' as title',
             'news.thumbnail',
             'news.description_'.App::getLocale().' as description',
             'users.name as user',
             'news.created_at'
         ])->where('news.status',1)->limit(4)->get();


         $cate = PropertyTypesModel::leftJoin('property_detail','property_detail.property_types_id','property_types.id')
            ->leftJoin('property','property.id','property_detail.property_id')
            ->select([
                'property_types.name_'.App::getLocale().' as name',
                DB::raw('count(property.id) as total'),
                'property_types.id'
                ])
            ->where('property_types.status',1)
            //->groupBy('property.id')
            ->groupBy('property_types.id')
            ->groupBy('property_detail.property_types_id')
            //->groupBy('property.id')
            ->orderBy('total','DESC');
            $category = $cate->get();

//dd($category);
        return view('front-end.page.property',compact('news_latest','property','property_type','general_amenities','latest_property','feature_property','category','gallery_detail'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

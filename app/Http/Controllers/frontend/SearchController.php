<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PropertyModel;
use App\Models\PropertyDetailModel;
use App\Models\PropertyTypesModel;
use App\Models\PropertyAmenitiesDetailModel;
use App\Models\GeneralAmenitiesModel;
use App\Models\StatusModel;
use App\Models\TypesModel;
use App\Models\LocationModel;
use App;
use DB;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {

        $property       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->LeftJoin('property_detail','property_detail.property_id','property.id')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'property.types_id',
            'property.status_id',
            'status.name_'.App::getLocale().' as status_name',
            'users.name as user',
            'property.created_at'
        ])->where('property.status',1);
        //->groupBy('property.sale_price');

       // ->orderBy('property.id','DESC');
        if($request->types != 0){
            $property = $property->where('property.types_id',$request->types);
        }
        if($request->property_types != 0){
            $property = $property->where('property_detail.property_id',$request->property_types);
        }
        if($request->location != 0){
            $property = $property->where('property.location_id',$request->location);
        }
        if($request->bedroom != 0){
            $property = $property->where('property.bedroom_en',$request->bedroom);
        }
        if($request->bathroom != 0){
            $property = $property->where('property.bathroom_en',$request->bathroom);
        }
        if($request->status != 0){
            $property = $property->where('property.status_id',$request->status);
        }
        //search
        if($request->ordering == 'newest_first'){
            $property = $property->OrderBy('property.id','DESC');
        }
        if($request->ordering == 'date_added'){
            $property = $property->OrderBy('property.id','ASC');
        }
        if($request->ordering == 'lowest_price'){
            $property = $property->OrderBy('property.sale_price','ASC');
        }
        if($request->ordering == 'highest_price'){
            $property = $property->OrderBy('property.sale_price','DESC');
        }

        $property = $property->groupBy('property.id');
        $property = $property->paginate(6);
        $count = $property->count()-6;

        //feature_property
        $feature_property       = PropertyModel::Join('location','location.id','property.location_id')
        ->Join('types','types.id','property.types_id')
        ->Join('status','status.id','property.status_id')
        ->Join('users','users.id','property.created_by')
        ->select([
            'property.id',
            'property.title_'.App::getLocale().' as title',
            'property.sale_price','property.thumbnail',
            'property.description_'.App::getLocale().' as description',
            'location.name_'.App::getLocale().' as location',
            'property.land_size_'.App::getLocale().' as land_size',
            'property.building_size_'.App::getLocale().' as building_size',
            'property.parking_'.App::getLocale().' as parking',
            'property.bedroom_'.App::getLocale().' as bedroom',
            'property.bathroom_'.App::getLocale().' as bathroom',
            'property.living_room_'.App::getLocale().' as living_room',
            'property.legal_document_'.App::getLocale().' as legal_document',
            'property.property_owner_'.App::getLocale().' as property_owner',
            'types.name_'.App::getLocale().' as types_name',
            'status.name_'.App::getLocale().' as status_name',
            'property.created_at',
            'users.name as user'
        ])->where('property.status',1)
        ->where('property.feature_property','yes')
        ->orderBy('property.id','DESC')->limit(3)->get();

        $location = LocationModel::where('status',1)->get();
        $types    = TypesModel::where('status',1)->get();
        $status   = StatusModel::where('status',1)->get();
        $property_types = PropertyTypesModel::where('status',1)->get();
        return view('front-end.page.search',compact('property','count','feature_property','location','types','status','property_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

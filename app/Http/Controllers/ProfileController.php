<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CompanyModel;;
use Auth;
use Carbon\Carbon;
use Hash;
class ProfileController extends Controller
{
    /**
     * Your Profile
     */
    public function yourProfile(){
        $userLogin    = auth::user()->id;
        $profile      = User::where('id',$userLogin)->first();
        return view('admin.profile.your-profile',compact('profile'));
    }

    /**
     * yourProfileUpdate
     */
    public function yourProfileUpdate(request $request,$id){
       
        $id = auth::user()->id;
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email,'.$id,
        ]);
        if(\Request::input('old_password')) {
            $this->validate($request, [
                'old_password'     => 'required',
                'password'         => 'required|string|min:6|different:old_password',
                'confirm_password' => 'required_with:password|same:password|string|min:6'
            ]);
        }

        $input = User::find($id);
        $input->name = $request->name;
        if($request->hasFile('profile')) {
            $file             = $request->profile;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input->profile   = $name;
            $file->move(public_path('/upload'), $name);
        }
        
        if(\Request::input('old_password')) {
            if (Hash::check($request->get('old_password'), Auth::user()->password)) {
                $user = User::find(Auth::user()->id);
                $user->password = bcrypt($request->get('password'));
                if ($user->save()) {
                    return back()->with('success',trans('message.updated_successfully'));
                }
            } else {
                return back()->with('error',trans('message.wrong_old_password'));
            }
        }
        $input->save();
        return back()->with('success',trans('message.updated_successfully'));

    }
     /**
     * Company Profile
     */
    public function companyProfile(){
        $profile      = CompanyModel::where('status',1)->first();
        return view('admin.profile.company-profile',compact('profile'));
    }

    
     /**
     * companyProfileUpdate
     */
    public function companyProfileUpdate(request $request,$id){
       
        $this->validate($request, [
            'name_en'      => 'required',
            'name_kh'      => 'required',
            'phone'        => 'required',
            'address'      => 'required',
            'email'        => 'required|email',
        ]);
        

        $input = CompanyModel::find($id);
        $input->name_en = $request->name_en;
        $input->name_kh = $request->name_kh;
        $input->phone   = $request->phone;
        $input->address = $request->address;
        $input->email   = $request->email;
        if($request->hasFile('profile')) {
            $file             = $request->profile;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input->profile   = $name;
            $file->move(public_path('/upload'), $name);
        }

        $input->save();
        return back()->with('success',trans('message.updated_successfully'));

    }
}

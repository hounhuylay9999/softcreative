<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\PermissionModel;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.permission.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPermissionList(request $request){
        $length   = $request->get("pageLength");
        $role     = PermissionModel::where('status',1)->orderBy('id','desc')->take($length);
        return DataTables::of($role)
            ->addIndexColumn()
            ->addColumn('action',function ($role){
                $show = ''.$edit = ''.$delete = '';
               // if(auth::user()->can('role-list')):
                    $show = '<a  data-hint="View"  class="btn btn-square btn-sm btn-success hint--left hint--default" href=""><i class="fa fa-eye "></i></a> ';
              //  endif;
                    $edit = '<a  data-hint="Modify" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('roles.edit', Crypt::encrypt($role->id)).'"><i class="fa fa-edit "></i></a> ';
                    $delete = '<a  data-hint="Delete"  data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$role->id.')"><i class="fa fa-trash"></i></a> ';
                
                return $show.$edit.$delete;
            })
            ->addColumn('date',function ($role){
                return $role->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

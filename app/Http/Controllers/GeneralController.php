<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GeneralModel;
use DB;
use Carbon\Carbon;
class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ch = GeneralModel::where('status',1);
        if($ch->count() == 0){
            return view('back-end.general');
        }else{
            $general = $ch->first();
            return view('back-end.general-edit',compact('general'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'header_logo'           => 'required',
        ]);

        DB::beginTransaction();
        try{

            //store en
            $general_en = new GeneralModel();
            $general_en->title                           = $request->title;
            $general_en->slogan                          = $request->slogan;
            $general_en->phone                           = $request->phone;
            $general_en->fax                             = $request->fax;
            $general_en->email                           = $request->email;
            $general_en->address_en                      = $request->address_en;
            $general_en->address_kh                      = $request->address_kh ? $request->address_kh : $request->address_en;

            $general_en->meta_description_en             = $request->meta_description_en;
            $general_en->meta_description_kh             = $request->meta_description_kh ? $request->meta_description_kh : $request->meta_description_en;
            $general_en->meta_keyword_en                 = $request->meta_keyword_en;
            $general_en->meta_keyword_kh                 = $request->meta_keyword_kh ? $request->meta_keyword_kh : $request->meta_keyword_en;
            $general_en->description_en                  = $request->description_en;
            $general_en->description_kh                  = $request->description_kh ? $request->description_kh : $request->description_en;
            $general_en->status             = 1;
            if($request->hasFile('header_logo')) {
                $file             = $request->header_logo;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->header_logo   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('favicon')) {
                $file             = $request->favicon;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->favicon   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save
            $general_en->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{

            //store en
            $general_en =  GeneralModel::where('id',$id)->first();
            $general_en->title                           = $request->title;
            $general_en->slogan                          = $request->slogan;
            $general_en->phone                           = $request->phone;
            $general_en->email                           = $request->email;
            $general_en->fax                             = $request->fax;
            $general_en->address_en                      = $request->address_en;
            $general_en->address_kh                      = $request->address_kh ? $request->address_kh : $request->address_en;

            $general_en->meta_description_en             = $request->meta_description_en;
            $general_en->meta_description_kh             = $request->meta_description_kh ? $request->meta_description_kh : $request->meta_description_en;
            $general_en->meta_keyword_en                 = $request->meta_keyword_en;
            $general_en->meta_keyword_kh                 = $request->meta_keyword_kh ? $request->meta_keyword_kh : $request->meta_keyword_en;
            $general_en->description_en                  = $request->description_en;
            $general_en->description_kh                  = $request->description_kh ? $request->description_kh : $request->description_en;


            $general_en->facebook_url                           = $request->facebook_url;
            $general_en->youtube_url                            = $request->youtube_url;
            $general_en->twitter_url                            = $request->twitter_url;
            $general_en->google_url                             = $request->google_url;
            $general_en->telegram_url                           = $request->telegram_url;
            $general_en->linkedin_url                           = $request->linkedin_url;


            $general_en->status             = 1;
            if($request->hasFile('header_logo')) {
                $file             = $request->header_logo;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->header_logo   = $name;
                $file->move(public_path('/upload'), $name);
            }

            if($request->hasFile('favicon')) {
                $file             = $request->favicon;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->favicon   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save
            $general_en->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

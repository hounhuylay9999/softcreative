<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PropertyHightlightModel;
use Illuminate\Support\Facades\DB;

class ProtertyHightlightController extends Controller
{
    public function index()
    {
        $pro     = PropertyHightlightModel::where('status',1);
        if($pro->count() > 0){
            $pro = $pro->first();
            return view('back-end.propertyhightlight.edit',compact('pro'));
        }else{
            return view('back-end.propertyhightlight.create');
        }
    }


    public function store(Request $request)
    {

        DB::beginTransaction();
        try{
            $prop = new PropertyHightlightModel();
            $prop->description_en             = $request->description_en;
            $prop->description_kh             = $request->description_kh ? $request->description_kh : $request->description_en;
            $prop->title_quiet_en             = $request->title_quiet_en ;
            $prop->title_quiet_kh             = $request->title_quiet_kh ? $request->title_quiet_kh : $request->title_quiet_en ;
            $prop->description_quiet_en       = $request->description_quiet_en;
            $prop->description_quiet_kh       = $request->description_quiet_kh ? $request->description_quiet_kh : $request->description_quiet_en;
            $prop->title_great_en             = $request->title_great_en;
            $prop->title_great_kh             = $request->title_great_kh ? $request->title_great_kh : $request->title_great_en;
            $prop->description_great_en       = $request->description_great_en;
            $prop->description_great_kh       = $request->description_great_kh ? $request->description_great_kh : $request->description_great_en;
            $prop->title_view_en              = $request->title_view_en;
            $prop->title_view_kh              = $request->title_view_kh ? $request->title_view_kh : $request->title_view_en;
            $prop->description_view_en        = $request->description_view_en ;
            $prop->description_view_kh        = $request->description_view_kh ? $request->description_view_kh : $request->description_view_en ;
            $prop->title_large_en             = $request->title_large_en;
            $prop->title_large_kh             = $request->title_large_kh ? $request->title_large_kh : $request->title_large_en;
            $prop->description_large_en       = $request->description_large_en;
            $prop->description_large_kh       = $request->description_large_kh ? $request->description_large_kh : $request->description_large_en;
            $prop->status                     = 1;
            $prop->save();
            DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }




    public function update(Request $request,$id)
    {

        DB::beginTransaction();
        try{
            $prop =  PropertyHightlightModel::where('id',$id)->first();
            $prop->description_en             = $request->description_en;
            $prop->description_kh             = $request->description_kh ? $request->description_kh : $request->description_en;
            $prop->title_quiet_en             = $request->title_quiet_en ;
            $prop->title_quiet_kh             = $request->title_quiet_kh ? $request->title_quiet_kh : $request->title_quiet_en ;
            $prop->description_quiet_en       = $request->description_quiet_en;
            $prop->description_quiet_kh       = $request->description_quiet_kh ? $request->description_quiet_kh : $request->description_quiet_en;
            $prop->title_great_en             = $request->title_great_en;
            $prop->title_great_kh             = $request->title_great_kh ? $request->title_great_kh : $request->title_great_en;
            $prop->description_great_en       = $request->description_great_en;
            $prop->description_great_kh       = $request->description_great_kh ? $request->description_great_kh : $request->description_great_en;
            $prop->title_view_en              = $request->title_view_en;
            $prop->title_view_kh              = $request->title_view_kh ? $request->title_view_kh : $request->title_view_en;
            $prop->description_view_en        = $request->description_view_en ;
            $prop->description_view_kh        = $request->description_view_kh ? $request->description_view_kh : $request->description_view_en ;
            $prop->title_large_en             = $request->title_large_en;
            $prop->title_large_kh             = $request->title_large_kh ? $request->title_large_kh : $request->title_large_en;
            $prop->description_large_en       = $request->description_large_en;
            $prop->description_large_kh       = $request->description_large_kh ? $request->description_large_kh : $request->description_large_en;
            $prop->status                     = 1;
            $prop->save();
            DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }


}

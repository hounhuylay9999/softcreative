<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusModel;
use App\Models\TypesModel;
use App\Models\LocationModel;
use App\Models\PropertyTypesModel;
use App\Models\PropertyModel;
use App\Models\PropertyDetailModel;
use App\Models\PropertyGalleryModel;
use App\Models\GeneralAmenitiesModel;
use App\Models\PropertyAmenitiesDetailModel;
use App\Models\PropertyDetailGalleryModel;
use Yajra\DataTables\DataTables;
use DB;
use Carbon\Carbon;
use Auth;
class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back-end.property.index');
    }



    /**
     * get property listing
     */
    public function getPropertyList(request $request){
        $length   = $request->get("pageLength");
        $user     = PropertyModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('pt.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                return $edit.$delete;
            })
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location       = LocationModel::where('status',1)->get();
        $types          = TypesModel::where('status',1)->get();
        $status         = StatusModel::where('status',1)->get();
        $property_types = PropertyTypesModel::where('status',1)->get();
        $amenities      = GeneralAmenitiesModel::where('status',1)->get();
        return view('back-end.property.create',compact('location','types','status','property_types','amenities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'location'                => 'required',
            'title'                   => 'required',
            'sale_price'              => 'required',
            'types'                   => 'required',
            'status'                  => 'required',
            'land_size'               => 'required',
            'building_size'           => 'required',
            'thumbnail'               => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $property = new PropertyModel();
            $property->location_id             = $request->location;
            $property->sale_price           = $request->sale_price;
            $property->types_id             = $request->types;
            $property->status_id            = $request->status;
            $property->title_en             = $request->title;
            $property->title_kh             = $request->title_kh ? $request->title_kh : $request->title;
            $property->land_size_en         = $request->land_size;
            $property->land_size_kh         = $request->land_size_kh ? $request->land_size_kh : $request->land_size;
            $property->building_size_en     = $request->building_size;
            $property->building_size_kh     = $request->building_size_kh ? $request->building_size_kh : $request->building_size;
            $property->parking_en           = $request->parking;
            $property->parking_kh           = $request->parking_kh ? $request->parking_kh : $request->parking;
            $property->bedroom_en           = $request->bedroom;
            $property->bedroom_kh           = $request->bedroom_kh ? $request->bedroom_kh : $request->bedroom;
            $property->bathroom_en          = $request->bathroom;
            $property->bathroom_kh          = $request->bathroom_kh ? $request->bathroom_kh : $request->bathroom;
            $property->air_conditioner_en   = $request->air_conditioner;
            $property->air_conditioner_kh   = $request->air_conditioner_kh ? $request->air_conditioner_kh : $request->air_conditioner;
            $property->living_room_en       = $request->living_room;
            $property->living_room_kh       = $request->living_room_kh ? $request->living_room_kh : $request->living_room;
            $property->legal_document_en    = $request->legal_document;
            $property->legal_document_kh    = $request->legal_document_kh ? $request->legal_document_kh : $request->legal_document;
            $property->property_owner_en    = $request->property_owner;
            $property->property_owner_kh    = $request->property_owner_kh ? $request->property_owner_kh : $request->property_owner;
            $property->description_en       = $request->description;
            $property->description_kh       = $request->description_kh ? $request->description_kh : $request->description;
            $property->google_maps          = $request->google_maps;
            $property->status               = 1;
            $property->created_by           = Auth::user()->id;
            $property->feature_property     = $request->feature_property;

            if($request->hasFile('thumbnail')) {
                $file             = $request->thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $property->thumbnail   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save property
            $property->save();
            //detail property types
            foreach($request->property_types as $k => $val){
                $propertyDetail = new PropertyDetailModel();
                $propertyDetail->property_id       = $property->id;
                $propertyDetail->property_types_id = $val;
                $propertyDetail->save();
            }

            //detail general Amenities
            foreach($request->amenities as $k => $val){
                $propertyDetail = new PropertyAmenitiesDetailModel();
                $propertyDetail->property_id       = $property->id;
                $propertyDetail->amenities_id      = $val;
                $propertyDetail->save();
            }
            //gallery
            if($request->hasFile('images')) {
                foreach($request->images as $k => $v){
                    $img = new PropertyDetailGalleryModel();
                    $file             = $v;
                    $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name             = $timestamp. '-' .$file->getClientOriginalName();
                    $img->images      = $name;
                    $file->move(public_path('/upload'), $name);
                    $img->property_id = $property->id;
                    $img->save();
                }
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = LocationModel::where('status',1)->get();
        $types    = TypesModel::where('status',1)->get();
        $status   = StatusModel::where('status',1)->get();
        $property_types = PropertyTypesModel::where('status',1)->get();
        $property       = PropertyModel::where('id',$id)->first();
        $amenities      = GeneralAmenitiesModel::where('status',1)->get();
        $property_detail= PropertyDetailModel::where('property_id',$id)->get();
        $amenities_detail     = PropertyAmenitiesDetailModel::where('property_id',$id)->get();
        $img_gallery    =  PropertyDetailGalleryModel::where('property_id',$id)->get();

        return view('back-end.property.edit',compact('location','types','status','property_types','property','property_detail','amenities','amenities_detail','img_gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'location'                => 'required',
            'title'                   => 'required',
            'sale_price'              => 'required',
            'types'                   => 'required',
            'status'                  => 'required',
            'land_size'               => 'required',
            'building_size'           => 'required',
          //  'thumbnail'               => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $property =  PropertyModel::where('id',$id)->first();
            $property->location_id          = $request->location;
            $property->sale_price           = $request->sale_price;
            $property->types_id             = $request->types;
            $property->status_id            = $request->status;
            $property->title_en             = $request->title;
            $property->title_kh             = $request->title_kh ? $request->title_kh : $request->title;
            $property->land_size_en         = $request->land_size;
            $property->land_size_kh         = $request->land_size_kh ? $request->land_size_kh : $request->land_size;
            $property->building_size_en     = $request->building_size;
            $property->building_size_kh     = $request->building_size_kh ? $request->building_size_kh : $request->building_size;
            $property->parking_en           = $request->parking;
            $property->parking_kh           = $request->parking_kh ? $request->parking_kh : $request->parking;
            $property->bedroom_en           = $request->bedroom;
            $property->bedroom_kh           = $request->bedroom_kh ? $request->bedroom_kh : $request->bedroom;
            $property->bathroom_en          = $request->bathroom;
            $property->bathroom_kh          = $request->bathroom_kh ? $request->bathroom_kh : $request->bathroom;
            $property->air_conditioner_en   = $request->air_conditioner;
            $property->air_conditioner_kh   = $request->air_conditioner_kh ? $request->air_conditioner_kh : $request->air_conditioner;
            $property->living_room_en       = $request->living_room;
            $property->living_room_kh       = $request->living_room_kh ? $request->living_room_kh : $request->living_room;
            $property->legal_document_en    = $request->legal_document;
            $property->legal_document_kh    = $request->legal_document_kh ? $request->legal_document_kh : $request->legal_document;
            $property->property_owner_en    = $request->property_owner;
            $property->property_owner_kh    = $request->property_owner_kh ? $request->property_owner_kh : $request->property_owner;
            $property->description_en       = $request->description;
            $property->description_kh       = $request->description_kh ? $request->description_kh : $request->description;
            $property->google_maps          = $request->google_maps;
            $property->status               = 1;
            $property->updated_by           = Auth::user()->id;
            $property->feature_property     = $request->feature_property;

            if($request->hasFile('thumbnail')) {
                $file             = $request->thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $property->thumbnail   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save property
            $property->save();
            //detail property types
            PropertyDetailModel::where('property_id',$id)->delete();
            foreach($request->property_types as $k => $val){
                $propertyDetail = new PropertyDetailModel();
                $propertyDetail->property_id       = $id;
                $propertyDetail->property_types_id = $val;
                $propertyDetail->save();
            }

            //detail general amenities
            PropertyAmenitiesDetailModel::where('property_id',$id)->delete();
            foreach($request->amenities as $k => $val){
                $propertyAmenitiesDetail = new PropertyAmenitiesDetailModel();
                $propertyAmenitiesDetail->property_id       = $id;
                $propertyAmenitiesDetail->amenities_id      = $val;
                $propertyAmenitiesDetail->save();
            }

            //gallery
            //PropertyDetailGalleryModel::where('property_id',$id)->delete();
            if($request->hasFile('images')) {
                foreach($request->images as $k => $v){
                    $img = new PropertyDetailGalleryModel();
                    $file             = $v;
                    $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name             = $timestamp. '-' .$file->getClientOriginalName();
                    $img->images      = $name;
                    $file->move(public_path('/upload'), $name);
                    $img->property_id = $id;
                    $img->save();
                }
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  PropertyModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            PropertyDetailModel::where('property_id',$id)->delete();
            PropertyAmenitiesDetailModel::where('property_id',$id)->delete();
            PropertyDetailGalleryModel::where('property_id',$id)->delete();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
    /**
     * Remove gallery
     */
    public function destroyImg($id){
        DB::beginTransaction();
        try{
            PropertyDetailGalleryModel::where('id',$id)->delete();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\SlideModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('slide.index');
    }


    /**
     * get project list
     */
    public function getSlideList(request $request){
        $length   = $request->get("pageLength");
        $user     = SlideModel::with('get_response')->where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('slide.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';

                return $edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $response = SlideModel::where('status',1)->get();
        return view('slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'slide_name'                   => 'required',
            'img'                          => 'required',
        ]);

      //  dd($request->all());

        DB::beginTransaction();
        try{

            //dd($request->all());
            //store en
            $slide = new SlideModel();
            $slide->name                   = $request->slide_name;
            $slide->photo                  = $request->img;
            $slide->status                 = 1;
            $slide->created_by             = Auth::user()->id;
            $slide->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $project = ProjectModel::where('id',$id)->first();
        // $response = User::where('status',1)->get();
        // $slide = ProjectDetailModel::where('project_id',$id)->get();
        $response = SlideModel::where('id',$id)->first();
        return view('slide.edit',compact('response'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'project_name'                   => 'required',
    //         'price'                          => 'required',
    //         'response_by'                    => 'required',
    //         'team_develop'                   => 'required',
    //     ]);

    //   //  dd($request->all());

    //     DB::beginTransaction();
    //     try{

    //         //dd($request->all());
    //         //store en
    //         $project =  ProjectModel::where('id',$id)->first();
    //         $project->project_name         = $request->project_name;
    //         $project->price                = $request->price;
    //         $project->response_by_id       = $request->response_by;
    //         $project->status               = 1;
    //         $project->updated_by           = Auth::user()->id;
    //         $project->save();

    //         ProjectDetailModel::where('project_id',$id)->delete();
    //         foreach($request->team_develop as $k => $v){
    //             $project_detail = new ProjectDetailModel();
    //             $project_detail->project_id     = $id;
    //             $project_detail->developer_id	= $v;
    //             $project_detail->save();
    //         }

    //         DB::commit();
    //     }catch(\Exception $e){
    //         DB::rollback();
    //         return back()->with('warning','Something Went Wrong!');
    //     }
    //     return back()->with('success',trans('message.update_successfully'));
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try{
    //         $general_en =  ProjectModel::where('id',$id)->first();
    //         $general_en->status             = 0;
    //         $general_en->save();
    //         DB::commit();
    //     }catch(\Exception $e){
    //         DB::rollback();
    //         return back()->with('warning','Something Went Wrong!');
    //     }
    //     return back()->with('success',trans('message.delete_successfully'));
    // }
}

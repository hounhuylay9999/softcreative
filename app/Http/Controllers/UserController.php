<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\RoleModel;
use Hash;
use Carbon\Carbon;
use App\Models\GenderModel;
use DB;
class UserController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('permission:user-list');
        // $this->middleware('permission:user-create', ['only' => ['create','store']]);
        // $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        // $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * getUserList
     */
    public function getUserList(request $request){
        $length   = $request->get("pageLength");
        $roleId   = $request->input('role_id');

        $user     = User::with('get_roles')
            ->where('status',1)
            ->whereHas('get_roles', function ($query) use ($roleId) {
                if($roleId != 0){
                    return $query->where('id', $roleId);
                }
            })
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                if(auth::user()->can('user-list')):
                   // $show = '<a  data-hint="View"  class="btn btn-square btn-sm btn-success hint--left hint--default" href=""><i class="fa fa-eye "></i></a> ';
                endif;
                //if(auth::user()->can('user-edit')):
                    $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('users.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
               // endif;
               // if(auth::user()->can('user-delete')):
                    $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
              //  endif;
                return $show.$edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles    = RoleModel::select(['name'])->where('status',1)->get();
        $gender   = GenderModel::get();
        return view('admin.users.create',compact('roles','gender'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'thumbnail' => 'required',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|same:confirm-password',
            'roles'     => 'required',
            'gender'    => 'required',
        ]);
        $input = new User();
        $input->email          = $request->email;
        $input->name           = $request->name;
        $input->phone_number   = $request->phone_number;
        $input->fax            = $request->fax;
        $input->position       = $request->position;
        $input->entry_by       = auth::user()->id;
        $input->password       = Hash::make($input['password']);
        $input->gender         = $request->gender;

        if($request->hasFile('thumbnail')) {
            $file             = $request->thumbnail;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input->profile   = $name;
            $file->move(public_path('/upload'), $name);
        }
        $input->save();
        //assign roles
        $input->assignRole($request->roles);
        return redirect()->route('users.index')->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user     = User::find($id);
        $roles    = RoleModel::select(['name'])->where('status',1)->get();
        $userRole = $user->roles->pluck('name')->first();
        $gender   = GenderModel::get();
        return view('admin.users.modify',compact('user','roles','userRole','gender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email,'.$id,
            'password'  => 'same:confirm-password',
            'roles'     => 'required',

        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }
        $input['update_by'] = auth::user()->id;
        $user = User::find($id);
        if($request->hasFile('thumbnail')) {
            $file             = $request->thumbnail;
            $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name             = $timestamp. '-' .$file->getClientOriginalName();
            $input['profile'] = $name;
            $file->move(public_path('/upload'), $name);
        }

        $user->update($input);

        //delete and assign roles
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
            ->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //$id = Crypt::decrypt($id);
         User::where('id',$id)->update(['status'=>0]);
         return redirect()->route('users.index')->with('success',trans('message.delete_successfully'));
    }
}
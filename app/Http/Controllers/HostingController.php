<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HostingModel;
// use App\Models\User;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class HostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hosting.index');
    }


    /**
     * get project list
     */
    public function getHostingList(request $request){
        $length   = $request->get("pageLength");
        $user     = HostingModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('hosting.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';

                return $edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $response = User::where('status',1)->get();
        // $project = ProjectModel::where('status',1)->get();

        return view('hosting.create');
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'hosting_name'                   => 'required',
        ]);

      //  dd($request->all());

        DB::beginTransaction();
        try{

            //dd($request->all());
            //store en
            $hosting = new HostingModel();
            $hosting->hosting_name                    = $request->hosting_name;
            $hosting->status                         = 1;
            $hosting->created_by                     = Auth::user()->id;
            $hosting->save();

            // foreach($request->select_project as $k => $v){
            //     $project = new ProjectModel();
            //     $project->project_name      = $domain->id;
            //     $domain->project_id	= $v;
            //     $domain->save();
            // }
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        // var_dump($project);
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hosting = HostingModel::where('id',$id)->first(); 
        // $project = ProjectModel::where('status',1)->get();
        // $response = User::where('status',1)->get();
        // $project = ProjectModel::where('status',1)->get();
        // $project = ProjectModel::where('project_name',$id)->get();

        return view('hosting.edit',compact('hosting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'hosting'                   => 'required',
        ]);

      //  dd($request->all());
      //<h1>dd{{$domain->hosting_by}}</h1>

        DB::beginTransaction();
        try{

            //dd($request->all());
            //store en
            $hosting =  HostingModel::where('id',$id)->first();
            $hosting->hosting_name                    = $request->hosting;
            $hosting->status                         = 1;
            $hosting->save();


            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.update_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  HostingModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

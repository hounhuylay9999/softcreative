<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\GeneralAmenitiesModel;
use Yajra\DataTables\DataTables;
use DB;
use Carbon\Carbon;
use Auth;
class GeneralAmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back-end.generalAmenities.index');
        
    }
    public function getPropertyList(request $request){
        $length   = $request->get("pageLength");
        $user     = GeneralAmenitiesModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('ga.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                return $edit.$delete;
            })
            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.generalAmenities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // $this->validate($request, [
        //     'name'                   => 'required',
        // ]);
        DB::beginTransaction();
        try{
            // dd($request->all());
            $general_am = new GeneralAmenitiesModel();
            $general_am->name_en             = $request->name_en ;
            $general_am->name_kh             = $request->name_kh ? $request->name_kh : $request->name_en ;
            $general_am->status               = 1;
            $general_am->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $generalAm = GeneralAmenitiesModel::where('id',$id)->first();
        return view('back-end.generalAmenities.edit',compact('generalAm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $generalAm = GeneralAmenitiesModel::where('id',$id)->first();
            $generalAm->name_en             = $request->name_en ;
            $generalAm->name_kh             = $request->name_kh ? $request->name_kh : $request->name_en ;
            $generalAm->status               = 1;
            $generalAm->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  GeneralAmenitiesModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

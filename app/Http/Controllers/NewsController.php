<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsModel;
use App\Models\TagsModel;
use App\Models\NewsDetailModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back-end.news.index');
    }


      /**
     * getBannerList
     */
    public function getNewsList(request $request){
        $length   = $request->get("pageLength");
        $user     = NewsModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('ns.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';

                return $edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = TagsModel::where('status',1)->get();
        return view('back-end.news.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'                   => 'required',
            'thumbnail'               => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $property = new NewsModel();
            $property->title_en             = $request->title;
            $property->title_kh             = $request->title_kh ? $request->title_kh : $request->title;
            $property->description_en       = $request->description;
            $property->description_kh       = $request->description_kh ? $request->description_kh : $request->description;
            $property->url                  = $request->url;

            $property->status               = 1;
            $property->created_by           = Auth::user()->id;

            if($request->hasFile('thumbnail')) {
                $file             = $request->thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $property->thumbnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $property->save();


            //detail
            foreach($request->tags as $k => $v){
                $tag = new NewsDetailModel();
                $tag->news_id = $property->id;
                $tag->tags_id = $v;
                $tag->save();
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = TagsModel::where('status',1)->get();
        $news = NewsModel::where('id',$id)->first();
        $tags_detail = NewsDetailModel::where('news_id',$id)->get();
        return view('back-end.news.edit',compact('news','tags','tags_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'                   => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $property =  NewsModel::where('id',$id)->first();
            $property->title_en             = $request->title;
            $property->title_kh             = $request->title_kh ? $request->title_kh : $request->title;
            $property->description_en       = $request->description;
            $property->description_kh       = $request->description_kh ? $request->description_kh : $request->description;
            $property->url                  = $request->url;

            $property->status               = 1;
            $property->updated_by           = Auth::user()->id;

            if($request->hasFile('thumbnail')) {
                $file             = $request->thumbnail;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $property->thumbnail   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $property->save();
            //detail
            NewsDetailModel::where('news_id',$id)->delete();
            foreach($request->tags as $k => $v){
                $tag = new NewsDetailModel();
                $tag->news_id = $id;
                $tag->tags_id = $v;
                $tag->save();
            }
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  NewsModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            NewsDetailModel::where('news_id',$id)->delete();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

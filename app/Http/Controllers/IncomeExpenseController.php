<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IncomeExpenseModel;
use App\Models\TypeModel;
use App\Models\MethodModel;
use App\Models\User;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class IncomeExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('income_expense.index');
    }


    /**
     * get project list
     */
    public function getIncomeExpenseList(request $request){
        $length   = $request->get("pageLength");
        $user     = IncomeExpenseModel::with('get_response')
            
            // ->where('status',1)
            // ->orderBy('id','desc')
            // ->take($length);
            ->select('income.*','method.name_en',DB::raw('type.name_en as type_name'))
            ->join('method', 'method.id', 'income.method_id')
            ->join('type', 'type.id', 'income.type_id')
            ->where('income.status',1)
            ->orderBy('income.id','desc')
            ->get();

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('income_expense.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';

                return $edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = TypeModel::where('status',1)->get();
        $method = MethodModel::where('status',1)->get();
        $income_expense = IncomeExpenseModel::where('status',1)->get();
        // $hosting = HostingModel::where('status',1)->get(); ,compact('response', 'project','hosting')

        return view('income_expense.create',compact('type', 'method'));
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'receipt'                       => 'required',
            'usd'                           => 'required',
            'riel'                           => 'required',
            // 'type_id'                       => 'required',
            // 'method_id'                     => 'required',
            'date'                          => 'required',
            'des'                           => 'required',

        ]);

      //  dd($request->all());

        DB::beginTransaction();
        try{

            //dd($request->all());
            //store en
            $income = new IncomeExpenseModel();
            $income->receipt                         = $request->receipt;
            $income->method_id                      = $request->method_id;
            $income->type_id                        = $request->type_id;
            $income->amount_usd                      = $request->usd;
            $income->amount_khr                      = $request->riel;
            $income->date                           = $request->date;
            $income->description                     = $request->des;
            // $income->project_id                     = $request->select_project;
            $income->status                         = 1;
            $income->created_by                     = Auth::user()->id;

            if($request->hasFile('choose')) {
                $file             = $request->choose;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $income->file_name   = $name;
                $file->move(public_path('/upload'), $name);
            }
            $income->save();

            // foreach($request->select_project as $k => $v){
            //     $project = new ProjectModel();
            //     $project->project_name      = $domain->id;
            //     $domain->project_id	= $v;
            //     $domain->save();
            // }
            
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        // var_dump($project);
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $income = IncomeExpenseModel::where('id',$id)->first();
        $type = TypeModel::where('status',1)->get();
        $method = MethodModel::where('status',1)->get();

        return view('income_expense.edit',compact('income','type', 'method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'receipt'                       => 'required',
            'usd'                           => 'required',
            'riel'                          => 'required',
            'date'                         => 'required',
            'description'                => 'required',
        ]);

      //  dd($request->all());

        DB::beginTransaction();
        try{

            //dd($request->all());
            //store en
            $income =  IncomeExpenseModel::where('id',$id)->first();
            $income->receipt                            = $request->receipt;
            $income->amount_usd                         = $request->usd;
            $income->amount_khr                         = $request->riel;
            $income->method_id                          = $request->method_id;
            $income->type_id                            = $request->type_id;
            $income->date                               = $request->date;
            $income->description                        = $request->description;
            
            $income->status                         = 1;
            $income->created_by                     = Auth::user()->id;
            $income->save();

            

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.update_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  IncomeExpenseModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

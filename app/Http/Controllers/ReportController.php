<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IncomeExpenseModel;
use App\Models\TypeModel;
use App\Models\MethodModel;
use App\Models\User;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = TypeModel::where('status', 1)->get();
        $method = MethodModel::where('status', 1)->get();
    //    dd($type);
        return view('report.index', compact('type','method'));
    }


    /**
     * get project list
     */
    public function getReportList(request $request){
        $length   = $request->get("pageLength");
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $method = $request->method;
        $type = $request->type;
        $user     = IncomeExpenseModel::with('get_response', 'get_method', 'get_type');
        // $user->where('income.method_id', $method);
        // $user->where('income.type_id', $type);
        if($start_date !=""){
            $user->whereDate('income.date','>=',date('Y-m-d', strtotime($start_date)));
            // $user->whereDate('income.date',$start_date);
        }
        if($end_date !=""){
            $user->whereDate('income.date','<=',date('Y-m-d', strtotime($end_date)));
            // $user->whereDate('income.date',$end_date);
        }
        if($type !=0){
            $user->where('income.type_id', $type);
        }
        if($method !=0){
            $user->where('income.method_id', $method);
        }
        
        // ->whereDate('property.created_at','<=',date('Y-m-d', strtotime($request->current_date)));
        $user = $user->take($length);
        return DataTables::of($user)
            ->addIndexColumn()

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }
}

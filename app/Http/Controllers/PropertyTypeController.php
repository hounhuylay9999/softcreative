<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PropertyTypesModel;
use Yajra\DataTables\DataTables;
use DB;
use Carbon\Carbon;
use Auth;

class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back-end.propertytype.index');
    }
    /**
     * get propertytype listing
     */
    public function getPropertytypeList(request $request){
        $length   = $request->get("pageLength");
        $user     = PropertyTypesModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('pttype.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';
                return $edit.$delete;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.propertytype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // $this->validate($request, [
        //     'name'                   => 'required',
        // ]);
        DB::beginTransaction();
        try{
            // dd($request->all());
            $property = new PropertyTypesModel();
            $property->name_en             = $request->name_en ;
            $property->name_kh             = $request->name_kh ? $request->name_kh : $request->name_en ;
            $property->status               = 1;
            $property->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $propertytype = PropertyTypesModel::where('id',$id)->first();
        return view('back-end.propertytype.edit',compact('propertytype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $propertytype =  PropertyTypesModel::where('id',$id)->first();
            $propertytype->name_en             = $request->name_en ;
            $propertytype->name_kh             = $request->name_kh ? $request->name_kh : $request->name_en ;
            $propertytype->status               = 1;
            $propertytype->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $general_en =  PropertyTypesModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}

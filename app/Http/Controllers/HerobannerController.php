<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HerobannerModel;
use DB;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Auth;
class HerobannerController extends Controller
{
    //
    public function index()
    {
        return view('back-end.hero-banner.index');
    }


    /**
     * getBannerList
     */
    public function getBannerList(request $request){
        $length   = $request->get("pageLength");
        $user     = HerobannerModel::where('status',1)
            ->orderBy('id','desc')
            ->take($length);

        return DataTables::of($user)
            ->addIndexColumn()
            ->addColumn('action',function ($user){
                $show = ''.$edit = ''.$delete = '';
                $edit = '<a  data-hint="'.trans('message.modify').'" class="btn btn-square btn-sm btn-info hint--left hint--default" href="'.route('herobanner.edit',$user->id).'"><i class="fa fa-edit "></i></a> ';
                $delete = '<a data-hint="'.trans('message.delete').'" data-toggle="modal" data-target="#DeleteModal" class="btn btn-square btn-sm btn-danger hint--left hint--default text-white" onclick="deleteData('.$user->id.')"><i class="fa fa-trash"></i></a> ';

                return $edit.$delete;
            })

            ->addColumn('date',function ($user){
                return $user->created_at->format('F d, Y');
            })
            ->rawColumns(['action','date'])
            ->make(true);
    }


    //
    public function create()
    {
        return view('back-end.hero-banner.create');
    }

    public function store(request $request){
          //dd($request->all());
          $this->validate($request, [

            'banner'                 => 'required',
        ]);

        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $general_en = new HerobannerModel();
            $general_en->title_en           = $request->title_en;
            $general_en->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;

            $general_en->short_title_en           = $request->short_title_en;
            $general_en->short_title_kh           = $request->short_title_kh ? $request->short_title_kh : $request->short_title_en;

            $general_en->short_description_en           = $request->short_description_en;
            $general_en->short_description_kh           = $request->short_description_kh ? $request->short_description_kh : $request->short_description_en;

            $general_en->status             = 1;
            if($request->hasFile('banner')) {
                $file             = $request->banner;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->banner   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save
            $general_en->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.save_successfully'));
    }

    public function edit($id){
        $herobanner = HerobannerModel::where('id',$id)->first();
        return view('back-end.hero-banner.modify',compact('herobanner'));
    }

    public function update(request $request,$id){


        DB::beginTransaction();
        try{
            //dd($request->all());
            //store en
            $general_en =  HerobannerModel::where('id',$id)->first();
            $general_en->title_en           = $request->title_en;
            $general_en->title_kh           = $request->title_kh ? $request->title_kh : $request->title_en;

            $general_en->short_title_en           = $request->short_title_en;
            $general_en->short_title_kh           = $request->short_title_kh ? $request->short_title_kh : $request->short_title_en;

            $general_en->short_description_en           = $request->short_description_en;
            $general_en->short_description_kh           = $request->short_description_kh ? $request->short_description_kh : $request->short_description_en;


            $general_en->status             = 1;
            if($request->hasFile('banner')) {
                $file             = $request->banner;
                $timestamp        = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name             = $timestamp. '-' .$file->getClientOriginalName();
                $general_en->banner   = $name;
                $file->move(public_path('/upload'), $name);
            }

            //save
            $general_en->save();
        DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.updated_successfully'));
    }

    public function destroy($id){
        DB::beginTransaction();
        try{
            $general_en =  HerobannerModel::where('id',$id)->first();
            $general_en->status             = 0;
            $general_en->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return back()->with('warning','Something Went Wrong!');
        }
        return back()->with('success',trans('message.delete_successfully'));
    }
}